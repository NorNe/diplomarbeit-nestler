"""MORSE simulation scenario for a car-like robot to learn how to avoid obstacles with vision input"""
# RUN IN FULLSCREEN VM WITH:
# morse run -g 400x400+1520,616 PGSD_morse_script.py

import random
import math

from morse.builder import *

# ---------- simulation parameters ----------
bpymorse.set_speed(120, 0, 0)	# fps, logic_step_max, physics_step_max
# ONLY CHANGE FPS --> logic/physics step > 1 cause horrible flow results

# ---------- robot ----------
# Land robot
atrv = ATRV()
atrv.properties(GroundRobot = True)
x_pos = random.uniform(-19, 19)		# big_room: -19, 19
y_pos = random.uniform(-19, 19)		# big_room: -19, 19
atrv.translate(x = x_pos, y = y_pos)
# atrv.translate(x = 32, y = 0)
rot = math.pi + math.atan2(y_pos, x_pos)
atrv.rotate(z = rot)		# always face middle at start
# atrv.rotate(z = 3.14)		# in radians -> 180°
# atrv.rotate(z = math.pi)	# in radians -> 180°

# ---------- sensors ----------
# global pose sensor / world knowledge
# ONLY FOR TESTING - NO WORLD KNOWLEDGE ALLOWED IN LEARNING
# pose = Pose()
# pose.translate(z = 0.75)
# atrv.append(pose)

# camera
cam = VideoCamera()
cam.properties(cam_width = 320, cam_height = 240, cam_focal = 10.0, Vertical_Flip = False)
cam.frequency(120)		# capped by simulation frame rate --> bpymorse.set_speed
cam.translate(x = 0.5, y = 0, z = 0.7)
atrv.append(cam)

# semantic camera
# ONLY FOR TESTING - NO WORLD KNOWLEDGE ALLOWED IN LEARNING
semcam = SemanticCamera()
semcam.properties(cam_width = 320, cam_height = 240, cam_focal = 10.0, cam_far = 10.0, relative = True)
semcam.frequency(120)
semcam.translate(x = 0.5, y = 0, z = 0.7)
atrv.append(semcam)

# odometry sensor
odometry = Odometry()
atrv.append(odometry)

# ---------- actors ----------
# motion
motion = MotionVW()
atrv.append(motion)
motion.add_interface("socket")	# used via pymorse for "stop" service in learning & testing

# teleporter
teleport = Teleport()
atrv.append(teleport)

# ---------- ROS interface ----------
# default interface for all components = ros
atrv.add_default_interface('ros')

# ---------- environment ----------
env = Environment('big_room_double_obj.blend', fastmode = False)		# sim_env_1
# env = Environment('hallway.blend', fastmode = False)		# sim_env_2
# env = Environment('roundabout.blend', fastmode = False)		# sim_env_3
# env = Environment('doorways.blend', fastmode = False)
# env = Environment('flow_test.blend', fastmode = False)	# flow from rotation
# env = Environment('flow_test_2.blend', fastmode = False)	# flow from translation TUNNEL
# env = Environment('flow_test_3.blend', fastmode = False)	# flow from translation FRONTAL
# env = Environment('multibox.blend', fastmode = False)
# env = Environment('flow_measuring.blend', fastmode = False)

# env.select_display_camera(cam)		# displays cam-view in small pic-in-pic
env.set_camera_location([20, -4, 9])
env.set_camera_rotation([0.94, 0, 0.973])