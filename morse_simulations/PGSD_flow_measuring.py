#!/usr/bin/env python3
""" RUN EXPLICITLY WITH python3! """

import sys, time#, argparse
import threading, signal
import logging
import re
import math
import random as rnd
import numpy as np
import numpy.matlib as ml
import numpy.linalg as la

import roslib
import rospy
import pymorse		# uses SOCKET interface, components use ROS --> can only work with simulation
from transformations import quaternion_from_euler	# transformations.py in same folder because import issues

from std_msgs.msg import String				# semcam
from std_msgs.msg import Float32MultiArray	# flow
from geometry_msgs.msg import Twist			# motion
from geometry_msgs.msg import Pose			# teleport
from nav_msgs.msg import Odometry			# odometry

class PGSD_tester(threading.Thread):
	def __init__(self):
		# init variables
		threading.Thread.__init__(self)
		self.name = "PGSD_tester"
		self.isrunning = True
		
		# control variables
		self.state = ml.zeros((5, 1))				# state vector (column): [motion.lin, motion.ang, flow.left, flow.right, flow_diff_RL]
		self.steps = 200							# control steps per run
		self.step_count = 0							# counts control steps
		self.pol_param = np.matrix("-2.5; 2.5")		# policy parameters; read from learning logs later
		self.goal_state = np.matrix("2.4; 0; 0.0016")# goal state = [linSpeed max, angSpeed = 0, flow_diff_RL small --> obj_dist even]
		# ml.zeros creates MATRIX, np.zeros only array!
		
		# start ros node
		rospy.init_node(self.name)
		pymorse.Morse().deactivate("atrv.teleport")	# cannot move with teleport active
		
		# subscriptions
		# movement copy
		rospy.Subscriber("/atrv/motion", Twist, self.cb_motion)
		# optical flow estimate
		rospy.Subscriber("/output/subflows", Float32MultiArray, self.cb_flow)
		
		# publications
		# motor commands
		self.pub_motion = rospy.Publisher("/atrv/motion", Twist, queue_size = 1)
		# teleporter
		self.pub_tele = rospy.Publisher("/atrv/teleport", Pose, queue_size = 1)
		# state
		self.pub_state = rospy.Publisher("/atrv/state", Float32MultiArray, queue_size = 1)
		# policy parameters
		self.pub_pol_param = rospy.Publisher("/atrv/pol_param", Float32MultiArray, queue_size = 1)
	
		# init logging
		self.init_logging()
	# end __init__

	# controller function
	def run(self):
		while self.isrunning:
			# choose action
			phi = self.state[(2,3),0]					# features of current state (flow.left, flow.right)
			rotation = self.pol_param.T*phi				# calculation of action
			
			# limit speed to capabilities of flow sensor
			rotation = np.clip(rotation, -3, 3)
			if self.expected_flow_from_rotation(rotation) >= 0.1629:
				max_allowed_lin_motion = self.max_allowed_linear_motion_per_rotation(rotation)
			else:
				max_allowed_lin_motion = 2.4
			action = Twist()
			action.linear.x = max_allowed_lin_motion	# drive as fast as possible with current rotation
			action.angular.z = rotation
			
			# perform action (publish)
			self.logger.info("%s\t%s\t%s", rospy.get_time(), phi[0,0], phi[1,0])	# log current flow
			self.pub_motion.publish(action)
			rospy.sleep(math.fabs(rotation)/3*0.4 + 0.13)	# wait [.13, .53] for callbacks of new state
			self.step_count += 1
			
			# publish state and policy parameters for easy number viewing
			# PLOTTING: "rqt" in terminal
			pub_state_data = Float32MultiArray()
			for i in self.state:
				pub_state_data.data.append(i)
			self.pub_state.publish(pub_state_data)
			pub_pol_param_data = Float32MultiArray()
			for i in range(self.pol_param.shape[0]):
				for j in range(self.pol_param.shape[1]):
					pub_pol_param_data.data.append(self.pol_param[i,j])
			self.pub_pol_param.publish(pub_pol_param_data)
			
			# log test result and restart
			if self.step_count == self.steps:
				print("Logging complete")
				print("###################")
				print("## TESTING DONE ###")
				print("###################")
				pymorse.Morse().quit()
				self.isrunning = False
	# end run	

# ---------- helper functions ----------
	def init_logging(self):
		self.logger = logging.getLogger("morse." + __name__)
		self.logger.setLevel(logging.INFO)
		self.formatter = logging.Formatter("%(message)s")
		self.file_handler = None	# produces 1 log per run
		# create logfile
		self.logfile = "logs/flow_measurement.log"
		# close old logfile and handler
		if self.file_handler is not None:
			self.file_handler.close()
			self.logger.removeHandler(self.file_handler)
		# create new handler
		self.file_handler = logging.FileHandler(self.logfile, mode = "w")
		self.file_handler.setFormatter(self.formatter)
		self.file_handler.setLevel(logging.INFO)
		self.logger.addHandler(self.file_handler)
		# start actual logging
		print("Started logging to", self.logfile)
		self.logger.info("Policy Parameters:\t%s %s", self.pol_param[0,0], self.pol_param[1,0])
		self.logger.info("Control Steps:\t\t%s\n", self.steps)
		self.logger.info("Timestamp\tFlow left\tFlow right")
	# end init_logging
	
	def expected_flow_from_rotation(self, ang_veloc):
		r = math.fabs(ang_veloc)
		return 0.0065*r**4 - 0.0674*r**3 + 0.1692*r**2 + 0.1954*r + 0.0192
	# end expected_flow_from_rotation
	
	# MAX flow is expected --> from head-on COLLISION test --> flow in tunnel/at angle is smaller
	def expected_flow_from_translation(self, lin_veloc):
		t = math.fabs(lin_veloc)
		return -0.1343*t**2 + 0.6317*t - 0.0557
	# end expected_flow_from_translation
	
	# ONLY use if expected flow >= 0.1629
	# MAX ALLOWED FLOW = 0.85 --> sensor limit
	def max_allowed_linear_motion_per_rotation(self, ang_veloc):
		r = self.expected_flow_from_rotation(ang_veloc)
		return (0.6317 - math.sqrt(0.5372*r - 0.08749715))/0.2686
	# end max_allowed_linear_motion_per_rotation

# ---------- callbacks ----------
	def cb_motion(self, msg):
		# accumulate state vector
		self.state[0,0] = msg.linear.x
		self.state[1,0] = msg.angular.z
	# end cb_motion
	
	def cb_flow(self, msg):
		# direction doesn't matter, only magnitude of flow --> absolute value
		x_flow_left = 0
		y_flow_left = 0
		x_flow_right = 0
		y_flow_right = 0
		emd_count = int(len(msg.data)/2)		# msg.data = (x1,y1,x2,y2...)
		emd_count_2 = int(emd_count/2)
		# extract flow data from msg
		for i in range(0, emd_count, 2):
			x_flow_left += math.fabs(msg.data[i])
			y_flow_left += math.fabs(msg.data[i + 1])
		for i in range(emd_count, emd_count*2, 2):
			x_flow_right += math.fabs(msg.data[i])
			y_flow_right += math.fabs(msg.data[i + 1])
		# calculate current flow
		flow_left = math.sqrt((x_flow_left/emd_count_2)**2 + (y_flow_left/emd_count_2)**2)
		flow_right = math.sqrt((x_flow_right/emd_count_2)**2 + (y_flow_right/emd_count_2)**2)
		rot_flow = self.expected_flow_from_rotation(self.state[1,0])	# subtract rotation-induced flow
		# accumulate state vector
		self.state[2,0] = np.clip(flow_left - rot_flow, 0, 100)		# flow non-negative
		self.state[3,0] = np.clip(flow_right - rot_flow, 0, 100)	# flow non-negative
		flow_diff_RL = self.state[3, 0] - self.state[2, 0]
		if math.fabs(flow_diff_RL) < self.goal_state[2, 0]:
			flow_diff_RL = self.goal_state[2, 0]
		self.state[4,0] = flow_diff_RL
	# end cb_flow
	
# end class PGSD_learner

# ---------- main function ----------
if __name__ == "__main__":
	# parser = argparse.ArgumentParser(description = __doc__)
	# parser.add_argument("-r", "--robot", dest="robot", default="atrv", help = "What type of robot: atrv, mav", type=str)
	# parser.set_defaults(mode = 0)
	# args = parser.parse_args()
	# print args
	tester = PGSD_tester()
	
	def handler(signum, frame):
		print("Signal handler called with signal", signum)
		tester.isrunning = False
		sys.exit(0)
		# raise IOError("Couldn't open device!")
	
	signal.signal(signal.SIGINT, handler)
	tester.start()	# calls run()
	
	# don't exit main before threads are cleaned up
	while True:
		# print("main loop")
		time.sleep(1)
