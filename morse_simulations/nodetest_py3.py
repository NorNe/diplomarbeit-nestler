#!/usr/bin/env python3

import time
import rospy
import math
import numpy as np
# import tf.transformations
import pymorse

from std_msgs.msg import String
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry

rospy.init_node("node_test")

def cb_semcam(msg):
	global cb_count					# abuse as tmp
	global mean_flow_many_frames	# abuse as tmp
	global pos_x, pos_y
	global dist2obj
	# print(type(msg.data))
	# semcam = eval(msg.data)
	# print(type(semcam[0]))	# type of output: (list of) dictionary
	# print(semcam[0].keys())	# keys of dict entries
	# print(semcam)				# print list of dicts (seen objects)
	# for dicts in semcam:
		# print(dicts["position"][2])
		# print(dicts["name"])
	# print("")
	
	# x_d = semcam[0]["position"][0] - pos_x
	# y_d = semcam[0]["position"][1] - pos_y
	# dist = math.sqrt(x_d**2 + y_d**2)
	# shift dist2obj history to left -> delete oldest
	# for i in range(len(dist2obj) - 1):
		# dist2obj[i] = dist2obj[i + 1]
	# add current dist2obj to history
	# dist2obj[4] = dist
	action.linear.x = 2.
	print(action)
	pub_motion.publish(action)
	time.sleep(1.)
	action.linear.x = 0.
	pub_motion.publish(action)
	print(action)
	time.sleep(5)
# end cb_semcam

def cb_odometry(msg):
	print(msg.twist.twist.linear.x)
	# print(action)
	pub_motion.publish(action)
# end cb_odometry

def cb_motion(msg):
	action.linear.x = msg.linear.x
	action.angular.z = msg.angular.z
# end cb_motion

def cb_pose(msg):
	global pos_x, pos_y
	pos = msg.pose.position
	pos_x = pos.x
	pos_y = pos.y
	# print(pos.x)
	# pymorse.Morse().sleep(2)
	# print(pos.y)
	# print(action)
	pub_motion.publish(action)
# end cb_pose

# global variables
cb_count = 0
mean_flow_many_frames = 0.
dist2obj = np.zeros((5, 1))
pos_x = 0.
pos_y = 0.
max_flow_left = 0.
max_flow_right = 0.

# publish motion
pub_motion = rospy.Publisher("/atrv/motion", Twist, queue_size = 1)
action = Twist()
action.linear.x = 2.
action.angular.z = -1
# pub_motion.publish(action)
"""
# publish teleport
pub_teleport = rospy.Publisher("/atrv/teleport", Pose)
start_pose = Pose()
start_pose.position.x = 15
start_pose.position.y = 0
start_pose.position.z = 0	# 0.85?
start_pose.orientation = tf.transformations.quaternion_from_euler(0, 0, 3.14)
pymorse.Morse().deactivate("/atrv/teleport")
"""
# rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_xy_rotation)
# rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_x_rotation)
# rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_xy_translation_t)
# rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_xy_translation_c)
# rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_diff_RL)
# rospy.Subscriber("/atrv/semcam", String, cb_semcam)
rospy.Subscriber("/atrv/motion", Twist, cb_motion)
rospy.Subscriber("/atrv/pose", PoseStamped, cb_pose)
# rospy.Subscriber("/atrv/odometry", Odometry, cb_odometry)

rospy.spin()
