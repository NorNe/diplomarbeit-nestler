#!/usr/bin/env python3
""" RUN EXPLICITLY WITH python3! """

import sys, time#, argparse
import threading, signal
import logging
import re
import math
import random as rnd
import numpy as np
import numpy.matlib as ml
import numpy.linalg as la

import rospy
import pymorse		# uses SOCKET interface, components use ROS --> can only work with simulation
from transformations import quaternion_from_euler	# transformations.py in same folder because import issues

from std_msgs.msg import String				# semcam
from std_msgs.msg import Float32MultiArray	# flow
from geometry_msgs.msg import Twist			# motion
from geometry_msgs.msg import Pose			# teleport
from nav_msgs.msg import Odometry			# odometry

class PGSD_tester(threading.Thread):
	def __init__(self):
		# init variables
		threading.Thread.__init__(self)
		self.name = "PGSD_tester"
		self.isrunning = True
		
		# control variables
		self.state = ml.zeros((5, 1))				# state vector (column): [motion.lin, motion.ang, flow.left, flow.right, flow_diff_RL]
		self.steps = 1000							# control steps per run
		self.step_count = 0							# counts control steps
		self.run_count = 0							# counts runs with current amount of steps
		self.max_runs = 5							# max amount of runs per steps
		self.pol_param = ml.zeros((2, 1))			# policy parameters; read from learning logs later
		self.speed_limit = 2.4*0.25
		self.goal_state = np.matrix("2.4; 0; 0.0016")# goal state = [linSpeed max, angSpeed = 0, flow_diff_RL small --> obj_dist even]
		self.cost = np.zeros(self.steps)			# costs of each step
		self.odo_hist = np.random.rand(10)			# motion.lin history of last 10 steps; used for collision detection
		# CHANGE: collision after 20, not 30 steps! --> ALSO IN RE-INIT
		self.semcam_dist_hist = np.ones(20)			# semcam dist2obj history of last 30 steps; used for collision detection
		self.semcam_obj_hist = np.chararray(20, itemsize = 20, unicode = True)# semcam obj-name history of last 30 steps; used for collision detection
		self.collision_step = -1					# IF collision is detected, save current step here for logging
		# ml.zeros creates MATRIX, np.zeros only array!
		
		# policy parameters of behaviors to test
		self.pp = np.empty([5,5,2])
		# 1 success
		self.pp[0,0,0] = -1.62714549258	# logs/alpha_0.3/PGSD_400steps_0.log
		self.pp[0,0,1] = 1.35673130714
		self.pp[0,1,0] = -1.20712002147	# logs/alpha_0.8/PGSD_500steps_3.log
		self.pp[0,1,1] = 1.17156110652
		self.pp[0,2,0] = -1.34138472392	# logs/alpha_1.1/PGSD_300steps_1.log
		self.pp[0,2,1] = 0.850659869172
		self.pp[0,3,0] = -4.43543948851	# logs/alpha_1.5/PGSD_800steps_4.log
		self.pp[0,3,1] = 1.1762084688
		self.pp[0,4,0] = -0.930260815413# logs/alpha_2.0/PGSD_900steps_4.log
		self.pp[0,4,1] = 3.75645233627
		# 2 successes
		self.pp[1,0,0] = -1.48598895311	# logs/alpha_0.2/PGSD_1000steps_4.log
		self.pp[1,0,1] = 1.42082941047
		self.pp[1,1,0] = -1.52988252259	# logs/alpha_0.7/PGSD_800steps_0.log
		self.pp[1,1,1] = 1.96330739371
		self.pp[1,2,0] = -1.25973337343	# logs/alpha_1.2/PGSD_700steps_0.log
		self.pp[1,2,1] = 3.28095553943
		self.pp[1,3,0] = -2.89497696667	# logs/alpha_1.6/PGSD_500steps_3.log
		self.pp[1,3,1] = 4.5218633036
		self.pp[1,4,0] = -4.11839769649	# logs/alpha_2.0/PGSD_1000steps_4.log
		self.pp[1,4,1] = 1.1730786221
		# 3 successes
		self.pp[2,0,0] = -2.19154643233	# logs/alpha_0.6/PGSD_700steps_0.log
		self.pp[2,0,1] = 1.11796820175
		self.pp[2,1,0] = -1.33946652149	# logs/alpha_1.0/PGSD_800steps_4.log
		self.pp[2,1,1] = 3.87896948119
		self.pp[2,2,0] = -1.90215848854	# logs/alpha_1.4/PGSD_700steps_4.log
		self.pp[2,2,1] = 4.37602993139
		self.pp[2,3,0] = -2.13358198202	# logs/alpha_1.7/PGSD_600steps_3.log
		self.pp[2,3,1] = 3.40417909637
		self.pp[2,4,0] = -1.37449756165	# logs/alpha_2.0/PGSD_900steps_0.log
		self.pp[2,4,1] = 2.20522911838
		# 4 successes
		self.pp[3,0,0] = -2.46330217977	# logs/alpha_0.4/PGSD_900steps_0.log
		self.pp[3,0,1] = 3.03984737828
		self.pp[3,1,0] = -3.83325118824	# logs/alpha_1.0/PGSD_800steps_0.log
		self.pp[3,1,1] = 3.16163577522
		self.pp[3,2,0] = -2.51003189234	# logs/alpha_1.3/PGSD_900steps_1.log
		self.pp[3,2,1] = 3.70183902754
		self.pp[3,3,0] = -2.01889997036	# logs/alpha_1.7/PGSD_400steps_2.log
		self.pp[3,3,1] = 3.8429672147
		self.pp[3,4,0] = -3.12845719127	# logs/alpha_2.0/PGSD_1000steps_2.log
		self.pp[3,4,1] = 2.12591372805
		# 5 successes
		self.pp[4,0,0] = -5.70565710213	# logs/alpha_0.3/PGSD_900steps_0.log
		self.pp[4,0,1] = 3.26778292405
		self.pp[4,1,0] = -3.78543072238	# logs/alpha_1.3/PGSD_800steps_2.log
		self.pp[4,1,1] = 4.16744841357
		self.pp[4,2,0] = -7.00460943891	# logs/alpha_1.7/PGSD_600steps_2.log
		self.pp[4,2,1] = 7.32361408357
		self.pp[4,3,0] = -1.62520306279	# logs/alpha_1.9/PGSD_300steps_3.log
		self.pp[4,3,1] = 3.28742114418
		self.pp[4,4,0] = -18.2358041586	# logs/alpha_2.0/PGSD_1000steps_3.log
		self.pp[4,4,1] = 16.542282427
		
		self.log_alpha = 0				# behaviour test: first number (success)
		self.log_inc_alpha = 1
		self.log_max_alpha = 4
		self.log_steps = 0				# behaviour test: second number (behaviour)
		self.log_min_steps = self.log_steps
		self.log_inc_steps = 1
		self.log_max_steps = 4
		self.log_run_count = 0			# learning log: run count
		self.log_max_run_count = 1
		self.read_next_learning_log()	# get first policy parameters
		
		# start ros node
		rospy.init_node(self.name)
		pymorse.Morse().deactivate("atrv.teleport")	# cannot move with teleport active
		
		# subscriptions
		# movement copy
		rospy.Subscriber("/atrv/motion", Twist, self.cb_motion)
		# optical flow estimate
		rospy.Subscriber("/output/subflows", Float32MultiArray, self.cb_flow)
		# semantic camera
		rospy.Subscriber("/atrv/semcam", String, self.cb_semcam)
		# odometry
		rospy.Subscriber("/atrv/odometry", Odometry, self.cb_odometry)
		
		# publications
		# motor commands
		self.pub_motion = rospy.Publisher("/atrv/motion", Twist, queue_size = 1)
		# teleporter
		self.pub_tele = rospy.Publisher("/atrv/teleport", Pose, queue_size = 1)
		# state
		self.pub_state = rospy.Publisher("/atrv/state", Float32MultiArray, queue_size = 1)
		# policy parameters
		self.pub_pol_param = rospy.Publisher("/atrv/pol_param", Float32MultiArray, queue_size = 1)
	
		# init logging
		self.init_logging()
	# end __init__

	# controller function
	def run(self):
		while self.isrunning:
			# choose action
			phi = self.state[(2,3),0]					# features of current state (flow.left, flow.right)
			rotation = self.pol_param.T*phi				# calculation of action
			
			# limit speed to capabilities of flow sensor
			rotation = np.clip(rotation, -3, 3)
			if self.expected_flow_from_rotation(rotation) >= 0.1629:
				max_allowed_lin_motion = self.max_allowed_linear_motion_per_rotation(rotation)
				max_allowed_lin_motion *= self.speed_limit/2.4	# scale down linear speed to turtlebot level
			else:
				max_allowed_lin_motion = self.speed_limit
			action = Twist()
			action.linear.x = max_allowed_lin_motion	# drive as fast as possible with current rotation
			action.angular.z = rotation
			
			# perform action (publish)
			self.pub_motion.publish(action)
			rospy.sleep(math.fabs(rotation)/3*0.4 + 0.13)	# wait [.13, .53] for callbacks of new state
			
			# calculate cost of new state
			# calculate cost of flow_diff_RL with sign, otherwise cost is greater for negatives
			self.goal_state[2,0] = math.copysign(self.goal_state[2,0], self.state[4,0])
			self.cost[self.step_count] = la.norm(self.state[(0,1,4),0] - self.goal_state)
			
			# check for collision conditions
			collision = False
			# too slow REAL linear motion --> wall collision
			if np.all(self.odo_hist < 0.09):
				collision = True
				print("Wall Collision!")
			# too close to object for last 30 steps?
			if np.all(self.semcam_dist_hist < 0.9):
				# closest object was the same for last 30 steps? --> stuck behind box
				if np.all(self.semcam_obj_hist == self.semcam_obj_hist[0]):
					collision = True
					print("Box Collision!")
			# collision detected; end run with huge cost
			if collision:
				self.cost[self.step_count] = 1000000
				self.collision_step = self.step_count
				self.step_count = self.steps
			else:
				self.step_count += 1	# no collision detected; continue run
			
			# publish state and policy parameters for easy number viewing
			# PLOTTING: "rqt" in terminal
			pub_state_data = Float32MultiArray()
			for i in self.state:
				pub_state_data.data.append(i)
			self.pub_state.publish(pub_state_data)
			pub_pol_param_data = Float32MultiArray()
			for i in range(self.pol_param.shape[0]):
				for j in range(self.pol_param.shape[1]):
					pub_pol_param_data.data.append(self.pol_param[i,j])
			self.pub_pol_param.publish(pub_pol_param_data)
			
			# log test result and restart
			if self.step_count == self.steps:
				stop_time = rospy.get_time()
				print("-- TESTING STOPPED --")
				# self.logger.info("-- TESTING STOPPED --\n")
				# self.logger.info("Stop Time:\t\t%s", stop_time)
				# self.logger.info("Duration:\t\t%s", stop_time - self.start_time)
				# self.logger.info("Cost Matrix:")
				# for i in self.cost:
					# self.logger.info(i)
				# log mean cost, max cost etc. to combined logfile
				cost_sum = np.sum(self.cost)
				cost_mean = np.mean(self.cost)
				cost_median = np.median(self.cost)
				cost_max = np.amax(self.cost)	# 2.4 = first state -> not driving -> lin.speed error = 2.4
				cost_min = np.amin(self.cost)
				self.logger.warning("log%s%s %s %s %s %s %s %s", self.log_alpha, self.log_steps, cost_sum, cost_mean, cost_median, cost_max, cost_min, self.collision_step)
				print("Logging complete")
				self.run_count += 1			# run completed
				self.start_next_test_run()
	# end run	

# ---------- helper functions ----------
	def start_next_test_run(self):
		# all runs for current log completed?
		if self.run_count == self.max_runs:
			self.run_count = 0
			self.log_run_count += 1
			# all logs for current log_run_count completed?
			if self.log_run_count == self.log_max_run_count:
				self.log_run_count = 0
				self.log_steps += self.log_inc_steps
				# all logs for current log_steps completed?
				if self.log_steps > self.log_max_steps:
					self.log_steps = self.log_min_steps
					self.log_alpha = round(self.log_alpha + self.log_inc_alpha, 1)	# correct occasional rounding error bullshit
					# testing finished; CLOSE SIMULATION
					if self.log_alpha > self.log_max_alpha:
						self.log_alpha = 0
						if self.speed_limit > 0.3:
							self.speed_limit = 0.245
						else:
							print("###################")
							print("## TESTING DONE ###")
							print("###################")
							pymorse.Morse().quit()
							self.isrunning = False
			self.read_next_learning_log()		# get next policy parameters
		
		# re-init changing variables
		self.state = ml.zeros((5, 1))
		self.step_count = 0
		self.cost = np.zeros(self.steps)
		self.odo_hist = np.random.rand(10)
		self.semcam_dist_hist = np.ones(20)
		self.semcam_obj_hist = np.chararray(20, itemsize = 20, unicode = True)
		self.collision_step = -1
		
		# reset simulation with new start location
		with pymorse.Morse() as sim:
			# stop ATRV and deactivate motion actuator
			sim.atrv.motion.stop()
			sim.deactivate("atrv.motion")
			# reset simulation
			sim.reset()
			# activate teleport actuator
			sim.activate("atrv.teleport")
			# teleport to random place and rotate towards middle
			x_pos = rnd.uniform(-19, 19)
			y_pos = rnd.uniform(-19, 19)
			yaw = math.pi + math.atan2(y_pos, x_pos)
			startpos = Pose()
			startpos.position.x = x_pos
			startpos.position.y = y_pos
			startpos.position.z = 0.1
			quat = quaternion_from_euler(0, 0, yaw)
			startpos.orientation.x = quat[0]
			startpos.orientation.y = quat[1]
			startpos.orientation.z = quat[2]
			startpos.orientation.w = quat[3]
			self.pub_tele.publish(startpos)
			sim.sleep(1)
			# deactivate teleport actuator
			sim.deactivate("atrv.teleport")
			# activate motion actuator
			sim.activate("atrv.motion")
		
		# re-init logging
		# self.reinit_logging()
	# end start_next_learning_run
	
	def init_logging(self):
		self.logger = logging.getLogger("morse." + __name__)
		self.logger.setLevel(logging.INFO)
		self.formatter = logging.Formatter("%(message)s")
		self.file_handler = None	# produces 1 log per run
		combined_log_fh = logging.FileHandler("logs/performance_slow_tests.log", mode = "a")	# produces 1 log for all runs
		combined_log_fh.setFormatter(self.formatter)
		combined_log_fh.setLevel(logging.WARNING)
		self.logger.addHandler(combined_log_fh)
		# self.reinit_logging()
	# end init_logging
	
	def reinit_logging(self):
		# create log directory
		self.logpath = "logs/alpha_" + str(self.log_alpha)	# same as learning -> should exist
		# create logfile
		self.logfile = self.logpath + "/PGSD_" + str(self.log_steps) + "steps_" + str(self.log_run_count) + "_test" + str(self.run_count) + ".log"
		# close old logfile and handler
		if self.file_handler is not None:
			self.file_handler.close()
			self.logger.removeHandler(self.file_handler)
		# create new handler
		self.file_handler = logging.FileHandler(self.logfile, mode = "w")
		self.file_handler.setFormatter(self.formatter)
		self.file_handler.setLevel(logging.INFO)
		self.logger.addHandler(self.file_handler)
		# start actual logging
		print("Started logging to", self.logfile)
		self.logger.info("Policy Parameters:\t%s %s", self.pol_param[0,0], self.pol_param[1,0])
		self.logger.info("Control Steps:\t\t%s", self.steps)
		self.start_time = rospy.get_time()
		self.logger.info("Start Time:\t\t%s\n", self.start_time)
	# end reinit_logging
	
	def read_next_learning_log(self):
		self.pol_param[0,0] = self.pp[self.log_alpha, self.log_steps, 0]
		self.pol_param[1,0] = self.pp[self.log_alpha, self.log_steps, 1]
	# end read_next_learning_log
	
	def expected_flow_from_rotation(self, ang_veloc):
		r = math.fabs(ang_veloc)
		return 0.0065*r**4 - 0.0674*r**3 + 0.1692*r**2 + 0.1954*r + 0.0192
	# end expected_flow_from_rotation
	
	# MAX flow is expected --> from head-on COLLISION test --> flow in tunnel/at angle is smaller
	def expected_flow_from_translation(self, lin_veloc):
		t = math.fabs(lin_veloc)
		return -0.1343*t**2 + 0.6317*t - 0.0557
	# end expected_flow_from_translation
	
	# ONLY use if expected flow >= 0.1629
	# MAX ALLOWED FLOW = 0.85 --> sensor limit
	def max_allowed_linear_motion_per_rotation(self, ang_veloc):
		r = self.expected_flow_from_rotation(ang_veloc)
		return (0.6317 - math.sqrt(0.5372*r - 0.08749715))/0.2686
	# end max_allowed_linear_motion_per_rotation

# ---------- callbacks ----------
	def cb_motion(self, msg):
		# accumulate state vector
		self.state[0,0] = msg.linear.x*2.4/self.speed_limit	# scale up linear speed to simulation level
		self.state[1,0] = msg.angular.z
	# end cb_motion
	
	def cb_flow(self, msg):
		# direction doesn't matter, only magnitude of flow --> absolute value
		x_flow_left = 0
		y_flow_left = 0
		x_flow_right = 0
		y_flow_right = 0
		emd_count = int(len(msg.data)/2)		# msg.data = (x1,y1,x2,y2...)
		emd_count_2 = int(emd_count/2)
		# extract flow data from msg
		for i in range(0, emd_count, 2):
			x_flow_left += math.fabs(msg.data[i])
			y_flow_left += math.fabs(msg.data[i + 1])
		for i in range(emd_count, emd_count*2, 2):
			x_flow_right += math.fabs(msg.data[i])
			y_flow_right += math.fabs(msg.data[i + 1])
		# calculate current flow
		flow_left = math.sqrt((x_flow_left/emd_count_2)**2 + (y_flow_left/emd_count_2)**2)
		flow_right = math.sqrt((x_flow_right/emd_count_2)**2 + (y_flow_right/emd_count_2)**2)
		rot_flow = self.expected_flow_from_rotation(self.state[1,0])	# subtract rotation-induced flow
		# accumulate state vector
		self.state[2,0] = np.clip(flow_left - rot_flow, 0, 10)	# flow non-negative
		self.state[3,0] = np.clip(flow_right - rot_flow, 0, 10)	# flow non-negative
		flow_diff_RL = self.state[3, 0] - self.state[2, 0]
		if math.fabs(flow_diff_RL) < self.goal_state[2, 0]:
			flow_diff_RL = self.goal_state[2, 0]
		self.state[4,0] = flow_diff_RL
	# end cb_flow

	def cb_semcam(self, msg):
		if self.step_count > 0:
			objects = eval(msg.data)
			min_dist = 100		# range of cam is 10
			min_obj = ""
			angle2box=0
			# find closest object in FOV
			for obj in objects:
				# non-empty keys: position, orientation, name
				# semcam in relative mode: x = horizontal diff to LoS, z = line of sight
				x = obj["position"][0]
				z = obj["position"][2]
				dist2box = math.sqrt(x**2 + z**2)
				angle2box = math.atan2(x, min_dist)
				if (dist2box < min_dist) and (-0.52 <= angle2box <= 0.52):	# limit FOV to +-30° (approx. width of flow detectors)
					min_dist = dist2box
					min_obj = obj["name"]
			self.semcam_dist_hist = np.roll(self.semcam_dist_hist, 1)	# overwrite oldest entry
			self.semcam_dist_hist[0] = min_dist
			self.semcam_obj_hist = np.roll(self.semcam_obj_hist, 1)		# overwrite oldest entry
			self.semcam_obj_hist[0] = min_obj
	# end cb_semcam
	
	def cb_odometry(self, msg):
		self.odo_hist = np.roll(self.odo_hist, 1)	# overwrite oldest entry
		self.odo_hist[0] = msg.twist.twist.linear.x
	# end cb_odometry
	
# end class PGSD_learner

# ---------- main function ----------
if __name__ == "__main__":
	# parser = argparse.ArgumentParser(description = __doc__)
	# parser.add_argument("-r", "--robot", dest="robot", default="atrv", help = "What type of robot: atrv, mav", type=str)
	# parser.set_defaults(mode = 0)
	# args = parser.parse_args()
	# print args
	tester = PGSD_tester()
	
	def handler(signum, frame):
		print("Signal handler called with signal", signum)
		tester.isrunning = False
		sys.exit(0)
		# raise IOError("Couldn't open device!")
	
	signal.signal(signal.SIGINT, handler)
	tester.start()	# calls run()
	
	# don't exit main before threads are cleaned up
	while True:
		# print("main loop")
		time.sleep(1)
