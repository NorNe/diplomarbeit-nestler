#!/usr/bin/env python
"""OpenCV feature detectors with ros CompressedImage Topics in python.

This example subscribes to a ros topic containing sensor_msgs 
CompressedImage. It converts the CompressedImage into a numpy.ndarray, 
then detects and marks features in that image. It finally displays 
and publishes the new image - again as CompressedImage topic.

Extended / modified to use raw uncompressed images
Option: convert ros image into a gstreamer source

"""
__author__ =  'Simon Haller <simon.haller at uibk.ac.at>'
__version__=  '0.1'
__license__ = 'BSD'
# Python libs
import sys, time, argparse

# numpy and scipy
import numpy as np
from scipy.ndimage import filters

# OpenCV
import cv2

# Ros libraries
import roslib
import rospy

# Ros Messages
from sensor_msgs.msg import Image
from std_msgs.msg import Int16MultiArray, Float32MultiArray, Float32, Int16
from rospy.numpy_msg import numpy_msg

VERBOSE=False

class image_processor:
	"""process ros image stream: feature finding, optical flow LK, optical flow EMDs farneback"""
	def __init__(self, robot):
		'''Initialize ros publisher, ros subscriber'''
		self.name = "image_processor"
		rospy.init_node('image_processor', anonymous=True)
		
		# topic where we publish
		self.image_pub = rospy.Publisher("/output/image_raw/compressed", Image)
		
		# subscribed Topic
		self.robot = robot
		topic_img = "/%s/cam/image" % self.robot
		self.sub_img = rospy.Subscriber(topic_img, Image, self.callback,  queue_size = 1)
		self.sub_ctrl = rospy.Subscriber("/ctrl/init_emd_pos", Int16, self.cb_init_emd_pos)
		self.cb_cnt = 0
		if VERBOSE :
			print "subscribed to %s" % topic_img
		self.initFrame = False
		self.doEMD = False
		self.doEMD2 = True
		self.width = 320	# MUST MATCH cam.properties in morse script
		self.height = 240	# MUST MATCH cam.properties in morse script
		
		# self.track_len = 10
		# self.detect_interval = 5
		# self.tracks = []
		# self.cam = video.create_capture(video_src)
		self.frame_idx = 0
		# subflows
		# self.flow_pub_x = rospy.Publisher("/output/subflow/x", Float32) # Float32MultiArray)
		# self.flow_pub_y = rospy.Publisher("/output/subflow/y", Float32) # Float32MultiArray)
		self.flow_pub = []
		self.numemds = 16		# 2x4 per side
		# for i in range(self.numemds):
		   # self.flow_pub.append(rospy.Publisher("/output/subflow/%d" % i, Float32MultiArray))
		self.flow8_pub = rospy.Publisher("/output/subflows", Float32MultiArray)
		# self.pub_emd_pos = rospy.Publisher("/emd/pos", Float32MultiArray)

		# EMD regions of interests
		self.emd_rois = []
		self.emd_size = 20	# 320x240 / ORG: 40
		# self.emd_size = 30	# 480x360
		# self.emd_size = 40	# 640x480
		self.emd_size_2 = self.emd_size/2
		self.init_EMDs_static3()
	
	# for further processing: even amount of EMDs, left side first
	def init_EMDs_static3(self):
		emd_points = np.array([], dtype = int).reshape(0,2)
		start_y = self.height*5/12
		end_y = self.height/2 + self.width/16
		# left side
		for x in range(self.width*3/16, self.width*3/8 + self.width/16, self.emd_size):
			for y in range(start_y, end_y, self.emd_size):
				emd_points = np.vstack((emd_points, [x, y]))
		# right side
		for x in range(self.width*9/16, self.width*3/4 + self.width/16, self.emd_size):
			for y in range(start_y, end_y, self.emd_size):
				emd_points = np.vstack((emd_points, [x, y]))
		# for 320x240:
		# emd_points = np.array(((60, 100), (60, 120), (80, 100), (80, 120), (100, 100), (100, 120), (120, 100), (120, 120), (180, 100), (180, 120), (200, 100), (200, 120), (220, 100), (220, 120), (240, 100), (240, 120)))
		# for 480x360:
		# emd_points = np.array(((90, 150), (90, 180), (120, 150), (120, 180), (150, 150), (150, 180), (180, 150), (180, 180), (270, 150), (270, 180), (300, 150), (300, 180), (330, 150), (330, 180), (360, 150), (360, 180)))
		# for 640x480:
		# emd_points = np.array(((120, 200), (120, 240), (160, 200), (160, 240), (200, 200), (200, 240), (240, 200), (240, 240), (360, 200), (360, 240), (400, 200), (400, 240), (440, 200), (440, 240), (480, 200), (480, 240)))
		# print emd_points
		self.emd_rois = []
		for point in emd_points:
			self.emd_rois.append(np.hstack((point, point + self.emd_size)))
		# print "self.emd_rois static2", self.emd_rois

	def init_EMDs_random(self):
		# pass
		self.emd_rois = []
		emd_points = np.zeros((self.numemds, 2), dtype=np.int)
		# emd_pos = Float32MultiArray()
		if self.robot == "atrv": # or rather: cam type forward
			emd_roi_xliml = 0 # lower x limit
			emd_roi_yliml = self.height/2 # lower y limit
		elif self.robot == "mav": # downward / full case
			emd_roi_xliml = 0 # lower x limit
			emd_roi_yliml = 0 # lower y limit
		for i in range(self.numemds):
			emd_points[i,0] = np.random.randint(0, self.width-self.emd_size)  # px
			emd_points[i,1] = np.random.randint(emd_roi_yliml, self.height-self.emd_size) # py
			self.emd_rois.append(np.hstack((emd_points[i], emd_points[i] + self.emd_size)))
		print "self.emd_rois random", self.emd_rois
		# emd_pos.data = self.emd_rois
		# print emd_pos

	def cb_init_emd_pos(self, ros_data):
		"""Trigger reinitialization of EMD positions"""
		if VERBOSE:
			print("received EMD position reinitialization trigger")
		emd_pos = Float32MultiArray()
		# self.init_EMDs_random()
		self.init_EMDs_static2()
		for i in range(self.numemds):
			p1 = self.emd_rois[i][0]
			p2 = self.emd_rois[i][1]
			emd_pos.data.append(p1)
			emd_pos.data.append(p2)
		self.pub_emd_pos.publish(emd_pos)
		
	def callback(self, ros_data):
		'''Callback function of subscribed topic. 
		Here images get converted and features detected'''
		if VERBOSE :
			print 'received image of type: "%s"' % ros_data.encoding

		#### direct conversion to CV2 ####
		# np_arr = np.fromstring(ros_data.data, np.uint8)
		np_arr = np.fromstring(ros_data.data, np.uint8)
		# image_np_f = np.reshape(np_arr, (240, 320, 4))
		image_np_f = np.reshape(np_arr, (self.height, self.width, 4))
		image_np = cv2.flip(image_np_f, 0)
		
		# convert np image to grayscale
		vis = image_np.copy()
		image_np_gray = cv2.cvtColor(image_np, cv2.COLOR_BGR2GRAY)

		if self.doEMD2:
			if self.initFrame:
				# get two consecutive frames
				img0, img1 = self.prev_gray, image_np_gray
				
				vel = []
				# for i in range(len(sr_x)):
				for i, point in enumerate(self.emd_rois):
					# print "point = ", point
					ir = img1[point[1]:point[3],point[0]:point[2]]
					ir_prev = img0[point[1]:point[3],point[0]:point[2]]
					flow = cv2.calcOpticalFlowFarneback(ir_prev, ir, 0, 3, 10, 10, 3, 1., 0)
					xvel = np.clip(np.sum(flow[:,:,0])/1600., -30., 30.)
					yvel = np.clip(np.sum(flow[:,:,1])/1600., -30., 30.)
					# self.flow_pub[i].publish(x = xvel, y = yvel)---------org------------
					# flow_pub_data = Float32MultiArray()
					# flow_pub_data.data.append(xvel)
					# flow_pub_data.data.append(yvel)
					# self.flow_pub[i].publish(flow_pub_data)
					# -----------------------
					vel.append(xvel)
					vel.append(yvel)
					# c1x = point[0] + 20 # sr_x[i] + 20
					# c1y = point[1] + 20 # sr_y[i] + 20
					c1x = point[0] + self.emd_size/2 # sr_x[i] + 20
					c1y = point[1] + self.emd_size/2 # sr_y[i] + 20
					cv2.line(vis, (c1x, c1y), (c1x + int(xvel * 10), c1y + int(yvel * 10)), (0, 0, 255, 0), 1)
					cv2.rectangle(vis, (point[0], point[1]), (point[2], point[3]), (0,0,255,0), 1)

				# self.flow8_pub.publish(flow = np.array(vel))----------org------------
				flow8_pub_data = Float32MultiArray()
				for i in vel:
					flow8_pub_data.data.append(i)
				self.flow8_pub.publish(flow8_pub_data)
				# --------------------
				cv2.imshow("cv_img_emd", vis)

		if self.doEMD:
			if self.initFrame:
				# get two consecutive frames
				img0, img1 = self.prev_gray, image_np_gray
				# define subrectangle coordinates
				sr_x = [self.r1[0][0], self.r2[0][0], self.r3[0][0], self.r4[0][0]]
				sr_y = [self.r1[0][1], self.r2[0][1], self.r3[0][1], self.r4[0][1]]
				vel = []
				for i in range(len(sr_x)):
					rect = [sr_x[i], sr_y[i]]
					ir = img1[rect[1]:rect[1]+self.emd_size,rect[0]:rect[0]+self.emd_size]
					ir_prev = img0[rect[1]:rect[1]+self.emd_size,rect[0]:rect[0]+self.emd_size]
					flow = cv2.calcOpticalFlowFarneback(ir_prev, ir, 0, 3, 10, 10, 3, 1., 0)
					xvel = np.clip(np.sum(flow[:,:,0])/1600., -30., 30.)
					yvel = np.clip(np.sum(flow[:,:,1])/1600., -30., 30.)
					self.flow_pub[i].publish(x = xvel, y = yvel)
					vel.append(xvel)
					vel.append(yvel)
					c1x = sr_x[i] + self.emd_size_2
					c1y = sr_y[i] + self.emd_size_2
					cv2.line(vis, (c1x, c1y), (c1x + int(xvel * 10), c1y + int(yvel * 10)), (0, 0, 255, 0), 1)
				self.flow8_pub.publish(flow = np.array(vel))
				
				cv2.rectangle(vis, self.r1[0], self.r1[1], (0,0,255,0), 1)
				cv2.rectangle(vis, self.r2[0], self.r2[1], (0,0,255,0), 1)
				cv2.rectangle(vis, self.r3[0], self.r3[1], (0,0,255,0), 1)
				cv2.rectangle(vis, self.r4[0], self.r4[1], (0,0,255,0), 1)
				cv2.imshow("cv_img_emd", vis)
		
		# cv2.waitKey(2)
		# cv2.waitKey(1)
		ch = 0xFF & cv2.waitKey(1)
		if ch == 27: # ESC
			# break
			sys.exit(1)
			
		self.prev_gray = image_np_gray
		self.initFrame = True

		#### Create Image ####
		msg = Image()
		msg.header.stamp = rospy.Time.now()
		msg.encoding = "rgba8"
		msg.data = vis.tostring()
		msg.step = self.width * 4 * 1	# ROS: Full row length in bytes --> RGBA = 4 bytes/px
		msg.width = self.width
		msg.height = self.height
		# Publish new image
		self.image_pub.publish(msg)
		self.cb_cnt += 1

def main(args):
	'''Initializes and cleanup ros node'''
	parser = argparse.ArgumentParser(description = "Simple random motion controller for car-like robot")
	parser.add_argument("-r", "--robot", dest="robot", default="atrv", help = "What type of robot: atrv, mav", type=str)
	# parser.set_defaults(mode = 0)
	
	args = parser.parse_args()
	print args

	# rospy.init_node('image_processor', anonymous=True)
	ic = image_processor(args.robot)
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print "Shutting down ROS Image feature detector module"
	cv2.destroyAllWindows()

if __name__ == '__main__':
	main(sys.argv)