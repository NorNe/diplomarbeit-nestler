#!/usr/bin/env python

import rospy
import logging
import math
import numpy as np
# import ast
import tf.transformations
# import pymorse

from std_msgs.msg import String
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped


rospy.init_node("node_test")
logging.basicConfig(filename='example.log',level=logging.DEBUG)
print "logger should be running"

def cb_semcam(msg):
	# print msg
	global cb_count					# abuse as tmp
	global mean_flow_many_frames	# abuse as tmp
	global pos_x, pos_y
	global dist2obj
	# print type(msg.data)
	# semcam = ast.literal_eval(msg.data)
	semcam = eval(msg.data)
	# print type(semcam[0])		# type of output: (list of) dictionary
	# print semcam[0].keys()	# keys of dict entries
	# print semcam				# print list of dicts (seen objects)
	# for dicts in semcam:
		# print dicts["position"]
	# print ""
	
	x_d = semcam[0]["position"][0] - pos_x
	y_d = semcam[0]["position"][1] - pos_y
	dist = math.sqrt(x_d**2 + y_d**2)
	# shift dist2obj history to left -> delete oldest
	for i in range(len(dist2obj) - 1):
		dist2obj[i] = dist2obj[i + 1]
	# add current dist2obj to history
	dist2obj[4] = dist
# end cb_semcam

def cb_motion(msg):
	# print msg
	# print msg.linear.x
	# print msg.angular.z
	action.linear.x = msg.linear.x
	action.angular.z = msg.angular.z
	# print "cb_motion", action.linear.x
# end cb_motion

def cb_pose(msg):
	# print msg
	global pos_x, pos_y
	pos = msg.pose.position
	pos_x = pos.x
	pos_y = pos.y
	logging.debug('This message should go to the log file')
	logging.info('So should this')
	logging.warning('And this, too')

# end cb_pose

# print "xy-flow rotation"
def cb_flow_xy_rotation(msg):
	# print "flow", msg
	global cb_count
	global mean_flow_many_frames
	cb_count += 1
	# direction doesn't matter, only magnitude of flow --> absolute value
	x_flow_left = 0
	y_flow_left = 0
	x_flow_right = 0
	y_flow_right = 0
	emd_count = len(msg.data)/2
	emd_count_2 = emd_count/2
	for i in range(0, emd_count, 2):
		x_flow_left += math.fabs(msg.data[i])
		y_flow_left += math.fabs(msg.data[i + 1])
	for i in range(emd_count, emd_count*2, 2):
		x_flow_right += math.fabs(msg.data[i])
		y_flow_right += math.fabs(msg.data[i + 1])
	
	if (cb_count == 1000):
		print action.angular.z, mean_flow_many_frames/1000
		cb_count = 0
		mean_flow_many_frames = 0
		action.angular.z += 0.1
	else:
		flow_left = math.sqrt((x_flow_left/emd_count_2)**2 + (y_flow_left/emd_count_2)**2)
		flow_right = math.sqrt((x_flow_right/emd_count_2)**2 + (y_flow_right/emd_count_2)**2)
		mean_flow_many_frames += (flow_left + flow_right)/2
	
	pub_motion.publish(action)
# end cb_flow_xy_rotation

# print "x-flow rotation"
def cb_flow_x_rotation(msg):
	# print "flow", msg
	global cb_count
	global mean_flow_many_frames
	cb_count += 1
	# direction doesn't matter, only magnitude of flow --> absolute value
	x_flow_left = 0
	x_flow_right = 0
	emd_count = len(msg.data)/2
	emd_count_2 = emd_count/2
	for i in range(0, emd_count, 2):
		x_flow_left += math.fabs(msg.data[i])
	for i in range(emd_count, emd_count*2, 2):
		x_flow_right += math.fabs(msg.data[i])
	
	if (cb_count == 1000):
		print action.angular.z, mean_flow_many_frames/1000
		cb_count = 0
		mean_flow_many_frames = 0
		action.angular.z += 0.1
	else:
		mean_flow_many_frames += (x_flow_left/emd_count_2 + x_flow_right/emd_count_2)/2
	
	pub_motion.publish(action)
# end cb_flow_x_rotation

# print "xy-flow translation TUNNEL"
def cb_flow_xy_translation_t(msg):
	# print "flow", msg
	global cb_count					# abuse as tmp
	global mean_flow_many_frames	# abuse as tmp
	global pos_x, pos_y
	global max_flow_left, max_flow_right
	# direction doesn't matter, only magnitude of flow --> absolute value
	x_flow_left = 0
	y_flow_left = 0
	x_flow_right = 0
	y_flow_right = 0
	emd_count = len(msg.data)/2
	emd_count_2 = emd_count/2
	"""
	# GLOBAL VARIABLES:
	# global cb_count		# abuse as repeat-counter for trips with same speed
	
	# GOAL --> restart
	# if pos_x < -11:
	if pos_x < 11:
		with pymorse.Morse() as sim:
			print action.linear.x, max_flow_left, max_flow_right
			max_flow_left = 0
			max_flow_right = 0
			# sim.quit()
			pymorse.Morse().deactivate("/atrv/motion")
			pymorse.Morse().activate("/atrv/teleport")
			pub_teleport.publish(start_pose)
			pymorse.Morse().deactivate("/atrv/teleport")
			pymorse.Morse().activate("/atrv/motion")
			# pymorse.Morse().reset_objects()
			# pymorse.Morse().sleep(0.1)	# wait for new camera frame to get correct flow
			cb_count += 1
			if cb_count > 4:
				action.linear.x += 0.1
				cb_count = 0
	# ON TRACK --> calculate flow to find trip max
	else:
		# print "i'm running!"
		for i in range(0, emd_count, 2):
			x_flow_left += math.fabs(msg.data[i])
			y_flow_left += math.fabs(msg.data[i + 1])
		for i in range(emd_count, emd_count*2, 2):
			x_flow_right += math.fabs(msg.data[i])
			y_flow_right += math.fabs(msg.data[i + 1])
		tmp_flow_left = math.sqrt((x_flow_left/emd_count)**2 + (y_flow_left/emd_count)**2)
		tmp_flow_right = math.sqrt((x_flow_right/emd_count)**2 + (y_flow_right/emd_count)**2)
		if (tmp_flow_left > max_flow_left):
			max_flow_left = tmp_flow_left
		if (tmp_flow_right > max_flow_right):
			max_flow_right = tmp_flow_right
	
	"""
	# GLOBAL VARIABLES:
	# global cb_count					# abuse as tmp for action.linear.x during return trip
	# global mean_flow_many_frames		# abuse as repeat-counter for trips with same speed
	
	# GOAL --> start return trip
	if (pos_x > 11) and (action.linear.x > 0):
		print action.linear.x, max_flow_left, max_flow_right
		cb_count = action.linear.x
		mean_flow_many_frames += 1
		action.linear.x = -20
	# RESTART --> inc speed
	elif (pos_x < -15.1) and (action.linear.x < 0):
		if mean_flow_many_frames > 4:
			action.linear.x = cb_count + 0.1
			mean_flow_many_frames = 0
		else:
			action.linear.x = cb_count
		max_flow_left = 0
		max_flow_right = 0
	# on track --> calculate flow to find max
	elif action.linear.x > 0:
		for i in range(0, emd_count, 2):
			x_flow_left += math.fabs(msg.data[i])
			y_flow_left += math.fabs(msg.data[i + 1])
		for i in range(emd_count, emd_count*2, 2):
			x_flow_right += math.fabs(msg.data[i])
			y_flow_right += math.fabs(msg.data[i + 1])
		tmp_flow_left = math.sqrt((x_flow_left/emd_count)**2 + (y_flow_left/emd_count)**2)
		tmp_flow_right = math.sqrt((x_flow_right/emd_count)**2 + (y_flow_right/emd_count)**2)
		if (tmp_flow_left > max_flow_left):
			max_flow_left = tmp_flow_left
		if (tmp_flow_right > max_flow_right):
			max_flow_right = tmp_flow_right
	# """
	pub_motion.publish(action)
# end cb_flow_xy_translation_t

# print "xy-flow translation COLLISION"
def cb_flow_xy_translation_c(msg):
	# print "flow", msg
	global cb_count					# abuse as tmp for action.linear.x during return trip
	global mean_flow_many_frames	# abuse as repeat-counter for trips with same speed
	global pos_x, pos_y
	global max_flow_left, max_flow_right
	global dist2obj
	# direction doesn't matter, only magnitude of flow --> absolute value
	x_flow_left = 0
	y_flow_left = 0
	x_flow_right = 0
	y_flow_right = 0
	emd_count = len(msg.data)/2
	emd_count_2 = emd_count/2
	
	# COLLISION --> start return trip
	if (action.linear.x > 0) and (dist2obj[4] < 1.13):#1.12):
		print action.linear.x, max_flow_left, max_flow_right
		cb_count = action.linear.x
		mean_flow_many_frames += 1
		action.linear.x = -10
	# RESTART --> inc speed
	elif (pos_x <= -10) and (action.linear.x < 0):
		if mean_flow_many_frames > 9:
			action.linear.x = cb_count + 0.1
			mean_flow_many_frames = 0
			# max_flow_left = 0
			# max_flow_right = 0
		else:
			action.linear.x = cb_count
		max_flow_left = 0
		max_flow_right = 0
	# on track --> calculate flow to find max
	elif action.linear.x > 0:
		for i in range(0, emd_count, 2):
			x_flow_left += math.fabs(msg.data[i])
			y_flow_left += math.fabs(msg.data[i + 1])
		for i in range(emd_count, emd_count*2, 2):
			x_flow_right += math.fabs(msg.data[i])
			y_flow_right += math.fabs(msg.data[i + 1])
		tmp_flow_left = math.sqrt((x_flow_left/emd_count)**2 + (y_flow_left/emd_count)**2)
		tmp_flow_right = math.sqrt((x_flow_right/emd_count)**2 + (y_flow_right/emd_count)**2)
		if (tmp_flow_left > max_flow_left):
			max_flow_left = tmp_flow_left
		if (tmp_flow_right > max_flow_right):
			max_flow_right = tmp_flow_right
	
	pub_motion.publish(action)
# end cb_flow_xy_translation_c

# print "flow_diff_RL"
def cb_flow_diff_RL(msg):
	global cb_count
	global pos_x, pos_y
	global mean_flow_many_frames	# abuse as sum for flow_diff_RL
	global dist2obj					# abuse as tmp for action.linear.x during return trip
	global max_flow_left			# abuse as repeat-counter for trips with same speed
	# direction doesn't matter, only magnitude of flow --> absolute value
	x_flow_left = 0
	y_flow_left = 0
	x_flow_right = 0
	y_flow_right = 0
	emd_count = len(msg.data)/2
	emd_count_2 = emd_count/2
	
	# calculate flow
	for i in range(0, emd_count, 2):
		x_flow_left += math.fabs(msg.data[i])
		y_flow_left += math.fabs(msg.data[i + 1])
	for i in range(emd_count, emd_count*2, 2):
		x_flow_right += math.fabs(msg.data[i])
		y_flow_right += math.fabs(msg.data[i + 1])
	tmp_flow_left = math.sqrt((x_flow_left/emd_count)**2 + (y_flow_left/emd_count)**2)
	tmp_flow_right = math.sqrt((x_flow_right/emd_count)**2 + (y_flow_right/emd_count)**2)
	
	# print mean flow_diff_RL after cb_count-many calls
	if cb_count >= 200:
		print action.linear.x, mean_flow_many_frames/cb_count
		cb_count = 0
		mean_flow_many_frames = 0.
		max_flow_left += 1
		dist2obj = action.linear.x
		action.linear.x = -10
	# on track --> calculate flow
	if action.linear.x > 0:
		mean_flow_many_frames += tmp_flow_right - tmp_flow_left
		cb_count += 1
	# RESTART --> inc speed after 10 runs
	elif (pos_x > 16) and (action.linear.x < 0):
		action.linear.x = 0
		pub_motion.publish(action)
		rospy.sleep(0.1)
		action.linear.x = dist2obj
		if max_flow_left >= 10:
			action.linear.x += 0.1
			max_flow_left = 0
	
	pub_motion.publish(action)
# end cb_flow_diff_RL

# global variables
cb_count = 0
mean_flow_many_frames = 0.
dist2obj = np.zeros((5, 1))
pos_x = 0.
pos_y = 0.
max_flow_left = 0.
max_flow_right = 0.

# publish motion
pub_motion = rospy.Publisher("/atrv/motion", Twist)
action = Twist()
action.linear.x = 0.5
action.angular.z = 0
"""
# publish teleport
pub_teleport = rospy.Publisher("/atrv/teleport", Pose)
start_pose = Pose()
start_pose.position.x = 15
start_pose.position.y = 0
start_pose.position.z = 0	# 0.85?
start_pose.orientation = tf.transformations.quaternion_from_euler(0, 0, 3.14)
pymorse.Morse().deactivate("/atrv/teleport")
"""
# sub = rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_xy_rotation)
# sub = rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_x_rotation)
# sub = rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_xy_translation_t)
# sub = rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_xy_translation_c)
# sub = rospy.Subscriber("/output/subflows", Float32MultiArray, cb_flow_diff_RL)
# sub = rospy.Subscriber("/atrv/semcam", String, cb_semcam)
# sub = rospy.Subscriber("/atrv/motion", Twist, cb_motion)
sub = rospy.Subscriber("/atrv/pose", PoseStamped, cb_pose)

rospy.spin()
