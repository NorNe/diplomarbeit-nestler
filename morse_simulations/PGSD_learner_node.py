#!/usr/bin/env python3
""" RUN EXPLICITLY WITH python3! """

import sys, time#, argparse
import threading, signal
import logging
import os.path
import math
import random
import numpy as np
import numpy.matlib as ml

import rospy
import pymorse		# uses SOCKET interface, components use ROS --> can only work with simulation/components need socket interface as well
from transformations import quaternion_from_euler	# transformations.py in same folder because import issues

from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose

class PGSD_learner(threading.Thread):
	def __init__(self):
		# init variables
		threading.Thread.__init__(self)
		self.name = "PGSD_learner"
		self.isrunning = True
		
		# PGSD variables
		self.state = ml.zeros((5, 1))				# state vector (column): [motion.lin, motion.ang, flow.left, flow.right, flow_diff_RL]
		self.S = np.matrix("1.0; -1.0; -1.0")		# signed derivative matrix
		self.H = 5									# time horizon
		self.alpha = 1.1				# START VALUE; learning rate
		self.inc_alpha = 0.1						# learning rate increment value
		self.max_alpha = 10.0						# max learning rate during runs
		self.steps = 1000				# START VALUE; learning steps; MUST BE DIVISIBLE BY SELF.H
		self.min_steps = self.steps					# min learning steps per run
		self.inc_steps = 100						# learning steps increment value
		self.max_steps = 1000						# max learning steps per run
		self.step_count = 0							# control step counter
		self.run_count = 0							# counts learning runs with current amount of steps
		self.max_runs = 5							# max amount of runs per steps
		self.pol_param = ml.zeros((2, 1))			# initial policy parameters
		self.goal_state = np.matrix("2.4; 0; 0.0016")# goal state = [linSpeed max, angSpeed = 0, flow_diff_RL small --> obj_dist even]
		
		# start ros node
		rospy.init_node(self.name)
		pymorse.Morse().deactivate("atrv.teleport")	# cannot move with teleport active
		
		# subscriptions
		# movement copy
		self.sub_motion = rospy.Subscriber("/atrv/motion", Twist, self.cb_motion)
		# optical flow estimate
		self.sub_flow = rospy.Subscriber("/output/subflows", Float32MultiArray, self.cb_flow)
		
		# publications
		# motor commands
		self.pub_motion = rospy.Publisher("/atrv/motion", Twist, queue_size = 1)
		# teleporter
		self.pub_tele = rospy.Publisher("/atrv/teleport", Pose, queue_size = 1)
		# state
		self.pub_state = rospy.Publisher("/atrv/state", Float32MultiArray, queue_size = 1)
		# policy parameters
		self.pub_pol_param = rospy.Publisher("/atrv/pol_param", Float32MultiArray, queue_size = 1)
	
		# init logging
		# self.init_logging()
	# end __init__

	# controller function
	def run(self):
		# PGSD algorithm
		while self.isrunning:
			state_H = ml.zeros((self.state.shape[0], self.H))	# next H states
			phi_H = ml.zeros((2, self.H))			# features of next H states

			# run policy for H steps
			for horizon in range(self.H):
				# choose action
				phi = self.state[(2,3),0]					# features of current state (flow.left, flow.right)
				rotation = self.pol_param.T*phi				# calculation of action
				phi_H[:, horizon] = phi						# save state features for parameter update
				
				# limit speed to capabilities of flow sensor
				rotation = np.clip(rotation, -3, 3)
				if self.expected_flow_from_rotation(rotation) >= 0.1629:
					max_allowed_lin_motion = self.max_allowed_linear_motion_per_rotation(rotation)
				else:
					max_allowed_lin_motion = 2.4
				action = Twist()
				action.linear.x = max_allowed_lin_motion	# drive as fast as possible with current rotation
				action.angular.z = rotation
				
				# perform action (publish)
				self.pub_motion.publish(action)
				rospy.sleep(math.fabs(rotation)/3*0.4 + 0.13)	# wait [.13, .53] for callbacks of new state
				state_H[:, horizon] = self.state				# save new state for parameter update
				
				# publish state and policy parameters for easy number viewing
				# PLOTTING: "rqt" in terminal, use PyQtGraph
				pub_state_data = Float32MultiArray()
				for i in self.state:
					pub_state_data.data.append(i)
				self.pub_state.publish(pub_state_data)
				pub_pol_param_data = Float32MultiArray()
				for i in range(self.pol_param.shape[0]):
					for j in range(self.pol_param.shape[1]):
						pub_pol_param_data.data.append(self.pol_param[i,j])
				self.pub_pol_param.publish(pub_pol_param_data)
			
			# policy parameter update (IF still learning)
			if self.step_count < self.steps:
				gradient = ml.zeros((self.pol_param.shape))
				for horizon in range(self.H):
					# prepare goal_state matrix: sign of flow_diff_RL must be same as in each saved state, otherwise error is greater for negative flow_diff_RL values
					goals_H = ml.zeros((state_H[(0,1,4),horizon:].shape))
					goals_H += self.goal_state
					for h in range(goals_H.shape[1]):
						goals_H[2,h] = math.copysign(goals_H[2,h], state_H[4,h + horizon])
					# gradient calculation
					# temp_gradient = (self.S.T * (state_H[(0,1,4),horizon:] - goals_H)).sum(axis = 1)
					temp_gradient = state_H[(0,1,4),horizon:] - goals_H
					t_g_L = np.matrix(temp_gradient)	# necessary, otherwise SAME OBJECT!
					t_g_R = np.matrix(temp_gradient)	# necessary, otherwise SAME OBJECT!
					t_g_L[0,:] *= -1	# L/R different signs, otherwise asymmetric pol_param = behaviour ***EXPLANATION AT END OF FILE
					t_g_R[0,:] *= 1		# 1/-1 = pol_param --> +-1 // -1/1 = pol_param --> +-inf
					t_g_L = (self.S.T * t_g_L).sum(axis = 1)
					t_g_R = (self.S.T * t_g_R).sum(axis = 1)
					# gradient += phi_H[:, horizon] * temp_gradient.T
					gradient[0,0] += phi_H[0, horizon] * t_g_L
					gradient[1,0] += phi_H[1, horizon] * t_g_R
				# actual parameter update
				self.pol_param -= self.alpha/self.H * gradient
				self.step_count += self.H	# add recent learning steps to counter
			# log learning result and restart
			elif self.step_count == self.steps:
				stop_time = rospy.get_time()
				print("-- LEARNING STOPPED --")
				# self.logger.info("-- LEARNING STOPPED --\n")
				# self.logger.info("Stop Time:\t\t\t%s", stop_time)
				# self.logger.info("Duration:\t\t\t%s", stop_time - self.start_time)
				# self.logger.info("Final Policy Parameters:\t%s %s", self.pol_param[0,0], self.pol_param[1,0])
				print("Logging complete")
				self.run_count += 1						# run completed
				self.start_next_learning_run()
	# end run	

# ---------- helper functions ----------
	def start_next_learning_run(self):
		# all runs for current steps completed?
		if self.run_count == self.max_runs:
			self.steps += self.inc_steps
			self.run_count = 0
		# all runs for current learning rate completed?
		if self.steps > self.max_steps:
			if self.alpha < self.max_alpha:
				self.alpha = round(self.alpha + self.inc_alpha, 1)	# correct occasional rounding error bullshit
				self.steps = self.min_steps
			# learning finished (min to max steps for learning rates min to max completed); CLOSE SIMULATION
			else:
				print("###################")
				print("## LEARNING DONE ##")
				print("###################")
				self.isrunning = False
				pymorse.Morse().quit()
		
		# re-init changing PGSD variables
		self.state = ml.zeros((5, 1))
		self.step_count = 0
		self.pol_param = np.matrix("0.0 ; 0.0")
		
		# reset simulation with new start location
		with pymorse.Morse() as sim:
			# stop ATRV and deactivate motion actuator
			sim.atrv.motion.stop()
			sim.deactivate("atrv.motion")
			# reset simulation
			sim.reset()
			# activate teleport actuator
			sim.activate("atrv.teleport")
			# teleport to random place and rotate towards middle
			x_pos = random.uniform(-19, 19)
			y_pos = random.uniform(-19, 19)
			yaw = math.pi + math.atan2(y_pos, x_pos)
			startpos = Pose()
			startpos.position.x = x_pos
			startpos.position.y = y_pos
			startpos.position.z = 0.1
			quat = quaternion_from_euler(0, 0, yaw)
			startpos.orientation.x = quat[0]
			startpos.orientation.y = quat[1]
			startpos.orientation.z = quat[2]
			startpos.orientation.w = quat[3]
			self.pub_tele.publish(startpos)
			sim.sleep(1)
			# deactivate teleport actuator
			sim.deactivate("atrv.teleport")
			# activate motion actuator
			sim.activate("atrv.motion")
		
		# re-init logging
		# self.reinit_logging()
	# end start_next_learning_run
	
	def init_logging(self):
		self.logger = logging.getLogger("morse." + __name__)
		self.file_handler = None
		self.logger.setLevel(logging.INFO)
		self.formatter = logging.Formatter("%(message)s")
		self.reinit_logging()
	# end init_logging
	
	def reinit_logging(self):
		# create log directory
		self.logpath = "logs/alpha_" + str(self.alpha)
		# self.logpath = "logs"
		if not os.path.isdir(self.logpath):
			os.mkdir(self.logpath)
		# create logfile
		self.logfile = self.logpath + "/PGSD_" + str(self.steps) + "steps_" + str(self.run_count) + ".log"
		# close old logfile and handler
		if self.file_handler is not None:
			self.file_handler.close()
			self.logger.removeHandler(self.file_handler)
		# create new handler
		self.file_handler = logging.FileHandler(self.logfile, mode = "w")
		self.file_handler.setFormatter(self.formatter)
		self.file_handler.setLevel(logging.INFO)
		self.logger.addHandler(self.file_handler)
		# start actual logging
		print("Started logging to", self.logfile)
		self.logger.info("Initial Policy Parameters:\t%s %s", self.pol_param[0,0], self.pol_param[1,0])
		self.logger.info("Learning Rate:\t\t\t%s", self.alpha)
		self.logger.info("Learning Steps:\t\t\t%s", self.steps)
		self.start_time = rospy.get_time()
		self.logger.info("Start Time:\t\t\t%s\n", self.start_time)
	# end reinit_logging
	
	def expected_flow_from_rotation(self, ang_veloc):
		r = math.fabs(ang_veloc)
		return 0.0065*r**4 - 0.0674*r**3 + 0.1692*r**2 + 0.1954*r + 0.0192
	# end expected_flow_from_rotation
	
	# MAX flow is expected --> from head-on COLLISION test --> flow in tunnel/at angle is smaller
	def expected_flow_from_translation(self, lin_veloc):
		t = math.fabs(lin_veloc)
		return -0.1343*t**2 + 0.6317*t - 0.0557
	# end expected_flow_from_translation
	
	# ONLY use if expected flow >= 0.1629
	# MAX ALLOWED FLOW = 0.85 --> sensor limit
	def max_allowed_linear_motion_per_rotation(self, ang_veloc):
		r = self.expected_flow_from_rotation(ang_veloc)
		return (0.6317 - math.sqrt(0.5372*r - 0.08749715))/0.2686
	# end max_allowed_linear_motion_per_rotation

# ---------- callbacks ----------
	def cb_motion(self, msg):
		# accumulate state vector
		self.state[0,0] = msg.linear.x
		self.state[1,0] = msg.angular.z
	# end cb_motion
	
	def cb_flow(self, msg):
		# direction doesn't matter, only magnitude of flow --> absolute value
		x_flow_left = 0
		y_flow_left = 0
		x_flow_right = 0
		y_flow_right = 0
		emd_count = int(len(msg.data)/2)		# msg.data = (x1,y1,x2,y2...)
		emd_count_2 = int(emd_count/2)
		# extract flow data from msg
		for i in range(0, emd_count, 2):
			x_flow_left += math.fabs(msg.data[i])
			y_flow_left += math.fabs(msg.data[i + 1])
		for i in range(emd_count, emd_count*2, 2):
			x_flow_right += math.fabs(msg.data[i])
			y_flow_right += math.fabs(msg.data[i + 1])
		# calculate current flow
		flow_left = math.sqrt((x_flow_left/emd_count_2)**2 + (y_flow_left/emd_count_2)**2)
		flow_right = math.sqrt((x_flow_right/emd_count_2)**2 + (y_flow_right/emd_count_2)**2)
		rot_flow = self.expected_flow_from_rotation(self.state[1,0])	# subtract rotation-induced flow
		# accumulate state vector
		self.state[2,0] = np.clip(flow_left - rot_flow, 0, 10)	# flow non-negative
		self.state[3,0] = np.clip(flow_right - rot_flow, 0, 10)	# flow non-negative
		flow_diff_RL = self.state[3,0] - self.state[2,0]
		if math.fabs(flow_diff_RL) < self.goal_state[2,0]:
			flow_diff_RL = self.goal_state[2,0]
		self.state[4,0] = flow_diff_RL
	# end cb_flow
# end class PGSD_learner

# ---------- main function ----------
if __name__ == "__main__":
	# parser = argparse.ArgumentParser(description = __doc__)
	# parser.add_argument("-r", "--robot", dest="robot", default="atrv", help = "What type of robot: atrv, mav", type=str)
	# parser.set_defaults(mode = 0)
	# args = parser.parse_args()
	# print args
	learner = PGSD_learner()
	
	def handler(signum, frame):
		print("Signal handler called with signal", signum)
		learner.isrunning = False
		sys.exit(0)
		# raise IOError("Couldn't open device!")
	
	signal.signal(signal.SIGINT, handler)
	learner.start()	# calls run()
	
	# don't exit main before threads are cleaned up
	while True:
		# print("main loop")
		time.sleep(1)


# ---------- EXPLANATION FOR GRADIENT CALCULATION ----------
# relevant lines:
# t_g_L[0,:] *= -1
# t_g_R[0,:] *= 1

# multiplication of linspeed for left side temp gradient with -1 is necessary because of sign
# rotation has sign, flow_diff_rl has sign, but linspeed is always positive
# --> asymmetric results for left(negative) and right(positive) pol_param
# --> linspeed * -1 in temp gradient for left side corrects that = symmetric behaviour