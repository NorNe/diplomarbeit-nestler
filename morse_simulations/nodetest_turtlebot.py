#!/usr/bin/env python3

import time
import os
import rospy
import math
import numpy as np
# import tf.transformations

from std_msgs.msg import String
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent

rospy.init_node("node_test")

def cb_odometry(msg):
	print(msg.twist.twist.linear.x)
	# print(action)
	pub_motion.publish(action)
# end cb_odometry

def cb_motion(msg):
	action.linear.x = msg.linear.x
	action.angular.z = msg.angular.z
# end cb_motion

def cb_bumper(self, msg):
	# collision detected
	if msg.state == 1:
		print("Bumper triggered: Collision!")
		os.system('read -p "Press any key to continue"')
# end cb_bumper

# global variables
cb_count = 0
mean_flow_many_frames = 0.
dist2obj = np.zeros((5, 1))
pos_x = 0.
pos_y = 0.
max_flow_left = 0.
max_flow_right = 0.

# publish motion
pub_motion = rospy.Publisher("/mobile_base/commands/velocity", Twist, queue_size = 1)
action = Twist()
action.linear.x = 2.
action.angular.z = 0

# rospy.Subscriber("/mobile_base/events/velocity", Twist, cb_motion)
rospy.Subscriber("/mobile_base/odom", Odometry, cb_odometry)
rospy.Subscriber("/mobile_base/events/Bumper", BumperEvent, cb_bumper)

rospy.spin()
