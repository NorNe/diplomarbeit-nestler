#!/usr/bin/env python

import re

counter = 0.0
ppL = 0.0
ppR = 0.0

for alpha in range(1, 21):
	for steps in range(100, 1100, 100):
		for run in range(5):
			log = "logs/alpha_" + str(float(alpha)/10) + "/PGSD_" + str(steps) + "steps_" + str(run) + ".log"
			with open(log, "r") as f:
				pol_param_line = f.readlines()[9]		# read 10th line
				pol_param_regex = re.compile("-?\d+.\d+")
				tmp = pol_param_regex.findall(pol_param_line)
				ppL += float(tmp[0])
				ppR += float(tmp[1])
				counter += 1

print("counter: ", counter)
print(ppL/counter, ppR/counter)
print("difference: ", (ppL+ppR)/counter)