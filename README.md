Code und Videos zur Diplomarbeit von Norbert Nestler.

Titel: Hindernisvermeidung bei mobilen Robotern mittels Reinforcement Learning

Kontakt: norbert_nestler@gmx.de

Videos in turtlebot_experiments -> videos

Kurzbeschreibung der Videos:

* example_run_1: kurze Demonstration von gelerntem Verhalten
* example_run_2: vollständiger Testlauf eines Verhaltens bis zur Kollision
* learning: TurtleBot beim Lernen; Versuchsaufbau nur zur Anschauung
* overactuated: TurtleBot reagiert zu heftig auf Sensoreingaben
* underactuated_1: TurtleBot reagiert zu gering
* underactuated_2: turtleBot reagiert zu gering; andere Ansicht

---------

Code and Videos of the diploma thesis of Norbert Nestler.

Title: Obstacle Avoidance for mobile Robots using Reinforcement Learning

Contact: norbert_nestler@gmx.de

Videos in turtlebot_experiments -> videos

Short Description of Videos:

* example_run_1: short demonstration of learned behavior
* example_run_2: complete test run until collision
* learning: TurtleBot learns; setup only for demonstrational purposes
* overactuated: TurtleBot reacts too strong on sensor input
* underactuated_1: TurtleBot reacts too soft
* underactuated_2: turtleBot reacts too soft; additional view