#!/usr/bin/env python

import time
import os
import rospy
import math
import numpy as np
# import tf.transformations

from std_msgs.msg import String
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent

rospy.init_node("node_test")

def cb_odometry(msg):
	print(msg.twist.twist.linear.x)
	# print(action)
	pub_motion.publish(action)
# end cb_odometry

def cb_motion(msg):
	# action.linear.x = msg.linear.x
	# action.angular.z = msg.angular.z
	# print(msg.linear.x)
	pub_motion.publish(action)
# end cb_motion

def cb_bumper(msg):
	global cb_count
	# print("---" + str(msg.state))
	# collision detected
	if msg.state == 1:
		# stop
		if action.linear.x != 0:
			print("Bumper triggered: TurtleBot stopped! Press Bumper again to continue.")
			cb_count += action.linear.x
			action.linear.x = 0
		# drive forward
		elif cb_count == 0:
			action.linear.x = 0.1
			print("continuing forward")
		# drive backward
		elif cb_count > 0:
			action.linear.x = -0.1
			print("continuing backward")
		pub_motion.publish(action)
# end cb_bumper

# global variables
cb_count = 0
mean_flow_many_frames = 0.
dist2obj = np.zeros((5, 1))
pos_x = 0.
pos_y = 0.
max_flow_left = 0.
max_flow_right = 0.

# publish motion
pub_motion = rospy.Publisher("/mobile_base/commands/velocity", Twist, queue_size = 1)
action = Twist()
# action.linear.x = 0.05
# action.angular.z = 0

rospy.Subscriber("/mobile_base/commands/velocity", Twist, cb_motion)
# rospy.Subscriber("/mobile_base/odom", Odometry, cb_odometry)
rospy.Subscriber("/mobile_base/events/bumper", BumperEvent, cb_bumper)

rospy.spin()
