#!/usr/bin/env python

import sys, time#, argparse
import threading, signal
import logging
import os.path
import re
import math
import numpy as np
import numpy.matlib as ml
import numpy.linalg as la

import rospy
from transformations import quaternion_from_euler	# transformations.py in same folder because import issues

from std_msgs.msg import Float32MultiArray	# flow
from geometry_msgs.msg import Twist			# motion
from kobuki_msgs.msg import BumperEvent		# bumper

class PGSD_tester(threading.Thread):
	def __init__(self):
		# init variables
		threading.Thread.__init__(self)
		self.name = "PGSD_turtlebot_tester"
		self.isrunning = True
		
		# control variables
		self.state = ml.zeros((5, 1))				# state vector (column): [motion.lin, motion.ang, flow.left, flow.right, flow_diff_RL]
		self.steps = 200							# control steps per run
		self.step_count = 0							# counts control steps
		self.pol_param = np.matrix("-2.5; 2.5")		# policy parameters; read from learning logs later
		self.tb_speed_limit = 0.7*0.35				# linear speed limit of turtlebot (0.7 hard cap)
		self.goal_state = np.matrix("2.4; 0; 0.0016")# goal state = [linSpeed max, angSpeed = 0, flow_diff_RL small --> obj_dist even]
		self.collision_step = -1					# IF collision is detected, save current step here for logging
		self.stopped = True							# start/stop switch with bumper
		self.bumper = -1							# ID of pressed bumper (0 = left, 1 = front, 2 = right)
		# ml.zeros creates MATRIX, np.zeros only array!
		
		# start ros node
		rospy.init_node(self.name)
		self.stop_time = rospy.get_time()			# time of last stop
		
		# subscriptions
		# movement copy
		rospy.Subscriber("/mobile_base/commands/velocity", Twist, self.cb_motion)
		# optical flow estimate
		rospy.Subscriber("/output/subflows", Float32MultiArray, self.cb_flow)
		# bumper
		rospy.Subscriber("/mobile_base/events/bumper", BumperEvent, self.cb_bumper)
		
		# publications
		# motor commands
		self.pub_motion = rospy.Publisher("/mobile_base/commands/velocity", Twist, queue_size = 1)
		# state
		self.pub_state = rospy.Publisher("/pgsd/state", Float32MultiArray, queue_size = 1)
		# policy parameters
		self.pub_pol_param = rospy.Publisher("/pgsd/pol_param", Float32MultiArray, queue_size = 1)
	
		# init logging
		self.init_logging()
	# end __init__

	# controller function
	def run(self):
		while self.isrunning:
			# choose action
			phi = self.state[(2,3),0]					# features of current state (flow.left, flow.right)
			if phi.shape[0] < 2:
					phi = phi.T
			rotation = self.pol_param.T*phi				# calculation of action
			
			# limit speed to capabilities of flow sensor/turtlebot
			rotation = np.clip(rotation, -3, 3)
			if self.expected_flow_from_rotation(rotation) >= 0.1629:
				max_allowed_lin_motion = self.max_allowed_linear_motion_per_rotation(rotation)
				max_allowed_lin_motion *= self.tb_speed_limit/2.4	# scale down linear speed to turtlebot level
			else:
				max_allowed_lin_motion = self.tb_speed_limit
			action = Twist()
			action.linear.x = max_allowed_lin_motion	# drive as fast as possible with current rotation
			action.angular.z = rotation
			
			# perform action (publish)
			if not self.stopped:
				self.logger.info("%s\t%s\t%s", rospy.get_time(), phi[0,0], phi[1,0])	# log current flow
				self.pub_motion.publish(action)
				rospy.sleep(math.fabs(rotation)/3*0.4 + 0.13)	# wait [.13, .53] for callbacks of new state
				self.step_count += 1
			
			# publish state and policy parameters for easy number viewing
			# PLOTTING: "rqt" in terminal
			pub_state_data = Float32MultiArray()
			for i in self.state:
				pub_state_data.data.append(i)
			self.pub_state.publish(pub_state_data)
			pub_pol_param_data = Float32MultiArray()
			for i in range(self.pol_param.shape[0]):
				for j in range(self.pol_param.shape[1]):
					pub_pol_param_data.data.append(self.pol_param[i,j])
			self.pub_pol_param.publish(pub_pol_param_data)
			
			# end run
			if self.step_count == self.steps:
				print("Logging complete")
				print("###################")
				print("## TESTING DONE ###")
				print("###################")
				self.stopped = True
				self.isrunning = False
	# end run	

# ---------- helper functions ----------
	def init_logging(self):
		self.logger = logging.getLogger("turtlebot." + __name__)
		self.logger.setLevel(logging.INFO)
		self.formatter = logging.Formatter("%(message)s")
		self.file_handler = None	# produces 1 log per run
		# create logfile
		self.logfile = "logs/flow_measurement.log"
		# close old logfile and handler
		if self.file_handler is not None:
			self.file_handler.close()
			self.logger.removeHandler(self.file_handler)
		# create new handler
		self.file_handler = logging.FileHandler(self.logfile, mode = "w")
		self.file_handler.setFormatter(self.formatter)
		self.file_handler.setLevel(logging.INFO)
		self.logger.addHandler(self.file_handler)
		# start actual logging
		print("Started logging to", self.logfile)
		self.logger.info("Policy Parameters:\t%s %s", self.pol_param[0,0], self.pol_param[1,0])
		self.logger.info("Control Steps:\t\t%s\n", self.steps)
		self.logger.info("Timestamp\tFlow left\tFlow right")
	# end init_logging
	
	def expected_flow_from_rotation(self, ang_veloc):
		r = math.fabs(ang_veloc)
		return 0.0065*r**4 - 0.0674*r**3 + 0.1692*r**2 + 0.1954*r + 0.0192
	# end expected_flow_from_rotation
	
	# MAX flow is expected --> from head-on COLLISION test --> flow in tunnel/at angle is smaller
	def expected_flow_from_translation(self, lin_veloc):
		t = math.fabs(lin_veloc)
		return -0.1343*t**2 + 0.6317*t - 0.0557
	# end expected_flow_from_translation
	
	# ONLY use if expected flow >= 0.1629
	# MAX ALLOWED FLOW = 0.85 --> sensor limit
	def max_allowed_linear_motion_per_rotation(self, ang_veloc):
		r = self.expected_flow_from_rotation(ang_veloc)
		return (0.6317 - math.sqrt(0.5372*r - 0.08749715))/0.2686
	# end max_allowed_linear_motion_per_rotation

# ---------- callbacks ----------
	def cb_motion(self, msg):
		# accumulate state vector
		self.state[0,0] = msg.linear.x*2.4/self.tb_speed_limit	# scale up linear speed to simulation level
		self.state[1,0] = msg.angular.z
	# end cb_motion
	
	def cb_flow(self, msg):
		# direction doesn't matter, only magnitude of flow --> absolute value
		emd_count = int(len(msg.data)/2)		# msg.data = (x1,y1,x2,y2...)
		emd_count_2 = int(emd_count/2)			# emds per side
		flow_left_mat = ml.zeros((1, emd_count_2))
		flow_right_mat = ml.zeros((1, emd_count_2))
		# extract flow data from msg
		for i in range(0, emd_count, 2):
			flow_left_mat[0,int(i/2)] = math.sqrt(msg.data[i]**2 + msg.data[i+1]**2)
		for i in range(emd_count, emd_count*2, 2):
			flow_right_mat[0,int((i-emd_count)/2)] = math.sqrt(msg.data[i]**2 + msg.data[i+1]**2)
		# filter outliers outside of mean +- 2*std.dev.
		threshold_left = np.mean(flow_left_mat) + 2*np.std(flow_left_mat)
		threshold_right = np.mean(flow_right_mat) + 2*np.std(flow_right_mat)
		for i in range(emd_count_2 - 1, -1, -1):
			if flow_left_mat[0,i] > threshold_left:
				flow_left_mat = np.delete(flow_left_mat, i)
			if flow_right_mat[0,i] > threshold_right:
				flow_right_mat = np.delete(flow_right_mat, i)
		# calculate current flow
		flow_left = np.mean(flow_left_mat)
		flow_right = np.mean(flow_right_mat)
		# subtract rotation-induced flow
		rot_flow = self.expected_flow_from_rotation(self.state[1,0])
		flow_left = np.clip(flow_left - rot_flow, 0, 10)	# flow non-negative, max <= 10
		flow_right = np.clip(flow_right - rot_flow, 0, 10)	# flow non-negative, max <= 10
		# accumulate state vector
		self.state[2,0] = flow_left
		self.state[3,0] = flow_right
		flow_diff_RL = self.state[3,0] - self.state[2,0]
		if math.fabs(flow_diff_RL) < self.goal_state[2,0]:
			flow_diff_RL = self.goal_state[2,0]
		self.state[4,0] = flow_diff_RL
	# end cb_flow
	
	def cb_bumper(self, msg):
		# bumper triggered
		if msg.state == 1:
			pause = rospy.get_time() - self.stop_time
			# check for manual bumper sequence to signal collision (left, front, right)
			if self.stopped:
				if self.bumper == -1 and msg.bumper == 0:
					self.bumper = msg.bumper
				elif self.bumper == 0 and msg.bumper == 1:
					self.bumper = msg.bumper
				elif self.bumper == 1 and msg.bumper == 2:
					self.cost[self.step_count] = 1000000
					self.collision_step = self.step_count
					self.step_count = self.steps		# setting step counter to max ends current run
					print("Bumper sequence detected: Collision! Trying to start next Test Run.")
				else:
					self.bumper = -1
					# resume driving
					if pause > 2.0:
						self.stopped = False
						print("Bumper triggered: Resuming to drive.")
			# stop for safety
			else:
				self.stopped = True
				self.stop_time = rospy.get_time()
				action = Twist()
				action.linear.x = 0.0
				action.angular.z = 0.0
				self.pub_motion.publish(action)
				print("Step: " + str(self.step_count))
				print("Bumper triggered: TurtleBot stopped! Press Bumper again to continue or left, front, right to signal Collision.")
	# end cb_bumper
	
# end class PGSD_tester

# ---------- main function ----------
if __name__ == "__main__":
	# parser = argparse.ArgumentParser(description = __doc__)
	# parser.add_argument("-r", "--robot", dest="robot", default="atrv", help = "What type of robot: atrv, mav", type=str)
	# parser.set_defaults(mode = 0)
	# args = parser.parse_args()
	# print args
	tester = PGSD_tester()
	
	def handler(signum, frame):
		print("Signal handler called with signal", signum)
		tester.isrunning = False
		sys.exit(0)
		# raise IOError("Couldn't open device!")
	
	signal.signal(signal.SIGINT, handler)
	tester.start()	# calls run()
	
	# don't exit main before threads are cleaned up
	while True:
		# print("main loop")
		time.sleep(1)
