#!/usr/bin/env python

import sys, time#, argparse
import threading, signal
import logging
import os.path
import math
import numpy as np
import numpy.matlib as ml

import rospy
from transformations import quaternion_from_euler	# transformations.py in same folder because import issues

from std_msgs.msg import Float32MultiArray	# flow
from geometry_msgs.msg import Twist			# motion
from kobuki_msgs.msg import BumperEvent		# bumper

class PGSD_learner(threading.Thread):
	def __init__(self):
		# init variables
		threading.Thread.__init__(self)
		self.name = "PGSD_turtlebot_learner"
		self.isrunning = True
		
		# PGSD variables
		self.state = ml.zeros((5, 1))				# state vector (column): [motion.lin, motion.ang, flow.left, flow.right, flow_diff_RL]
		self.S = np.matrix("1.0; -1.0; -1.0")		# signed derivative matrix
		self.H = 5									# time horizon
		self.alpha = 1.5							# learning rate
		self.steps = 500							# learning steps; MUST BE DIVISIBLE BY SELF.H
		self.step_count = 0							# step counter
		self.pol_param = ml.zeros((2, 1))			# initial policy parameters
		self.tb_speed_limit = 0.7*0.35				# linear speed limit of turtlebot (0.7 hard cap)
		self.goal_state = np.matrix("2.4; 0; 0.0016")# goal state = [linSpeed max, angSpeed = 0, flow_diff_RL small --> obj_dist even]
		self.stopped = True							# start/stop switch with bumper
		# ml.zeros creates MATRIX, np.zeros only array!
		
		# start ros node
		rospy.init_node(self.name)
		self.stop_time = rospy.get_time()			# time of last stop
		
		# subscriptions
		# movement copy
		rospy.Subscriber("/mobile_base/commands/velocity", Twist, self.cb_motion)
		# optical flow estimate
		rospy.Subscriber("/output/subflows", Float32MultiArray, self.cb_flow)
		# bumper
		rospy.Subscriber("mobile_base/events/bumper", BumperEvent, self.cb_bumper)
		
		# publications
		# motor commands
		self.pub_motion = rospy.Publisher("/mobile_base/commands/velocity", Twist, queue_size = 1)
		# state
		self.pub_state = rospy.Publisher("/pgsd/state", Float32MultiArray, queue_size = 1)
		# policy parameters
		self.pub_pol_param = rospy.Publisher("/pgsd/pol_param", Float32MultiArray, queue_size = 1)
	
		# init logging
		self.init_logging()
	# end __init__

	# controller function
	def run(self):
		# PGSD algorithm
		while self.isrunning:
			state_H = ml.zeros((self.state.shape[0], self.H))	# next H states
			phi_H = ml.zeros((2, self.H))			# features of next H states

			# run policy for H steps
			for horizon in range(self.H):
				# choose action
				phi = self.state[(2,3),0]					# features of current state (flow.left, flow.right)
				if phi.shape[0] < 2:
					phi = phi.T
				rotation = self.pol_param.T*phi				# calculation of action
				phi_H[:, horizon] = phi						# save state features for parameter update
				
				# limit speed to capabilities of flow sensor/turtlebot
				rotation = np.clip(rotation, -3, 3)
				if self.expected_flow_from_rotation(rotation) >= 0.1629:
					max_allowed_lin_motion = self.max_allowed_linear_motion_per_rotation(rotation)
					max_allowed_lin_motion *= self.tb_speed_limit/2.4	# scale down linear speed to turtlebot level
				else:
					max_allowed_lin_motion = self.tb_speed_limit
				action = Twist()
				action.linear.x = max_allowed_lin_motion	# drive as fast as possible with current rotation
				action.angular.z = rotation
				
				# perform action (publish)
				if not self.stopped:
					self.pub_motion.publish(action)
					rospy.sleep(math.fabs(rotation)/3*0.4 + 0.13)	# wait [.13, .53] for callbacks of new state
					state_H[:, horizon] = self.state				# save state for parameter update
					# log flow + timestamp for comparison with simulation
					# self.logger.info(rospy.get_time(), self.state[2,0], self.state[3,0])
				
				# publish state and policy parameters for easy number viewing
				# PLOTTING: "rqt" in terminal
				pub_state_data = Float32MultiArray()
				for i in self.state:
					pub_state_data.data.append(i)
				self.pub_state.publish(pub_state_data)
				pub_pol_param_data = Float32MultiArray()
				for i in range(self.pol_param.shape[0]):
					for j in range(self.pol_param.shape[1]):
						pub_pol_param_data.data.append(self.pol_param[i,j])
				self.pub_pol_param.publish(pub_pol_param_data)
			
			# policy parameter update (IF still learning)
			if (self.step_count < self.steps) and (not self.stopped):
				gradient = ml.zeros((self.pol_param.shape))
				for horizon in range(self.H):
					# prepare goal_state matrix: sign of flow_diff_RL must be same as in each saved state, otherwise error is greater for negatives
					goals_H = ml.zeros((state_H[(0,1,4),horizon:].shape))
					goals_H += self.goal_state
					for h in range(goals_H.shape[1]):
						goals_H[2,h] = math.copysign(goals_H[2,h], state_H[4,h + horizon])
					# gradient calculation
					# temp_gradient = (self.S.T * (state_H[(0,1,4),horizon:] - self.goal_state)).sum(axis = 1)
					temp_gradient = (state_H[(0,1,4),horizon:] - goals_H)
					t_g_L = np.matrix(temp_gradient)	# necessary, otherwise SAME OBJECT!
					t_g_R = np.matrix(temp_gradient)	# necessary, otherwise SAME OBJECT!
					t_g_L[0,:] *= -1	# L/R different signs, otherwise asymmetric pol_param = behaviour ***EXPLANATION AT END OF FILE
					t_g_R[0,:] *= 1		# 1/-1 = pol_param --> +-1 // -1/1 = pol_param --> +-inf
					t_g_L = (self.S.T * t_g_L).sum(axis = 1)
					t_g_R = (self.S.T * t_g_R).sum(axis = 1)
					# gradient += phi_H[:, horizon] * temp_gradient.T
					gradient[0,0] += phi_H[0, horizon] * t_g_L
					gradient[1,0] += phi_H[1, horizon] * t_g_R
				# actual parameter update
				self.pol_param -= self.alpha/self.H * gradient
				self.step_count += self.H	# add recent learning steps to counter for current run
			# log learning result and stop
			elif self.step_count == self.steps:
				stop_time = rospy.get_time()
				print("-- LEARNING STOPPED --")
				self.logger.info("-- LEARNING STOPPED --\n")
				self.logger.info("Stop Time:\t\t\t%s", stop_time)
				self.logger.info("Duration:\t\t\t%s", stop_time - self.start_time)
				self.logger.info("Final Policy Parameters:\t%s %s", self.pol_param[0,0], self.pol_param[1,0])
				print("Logging complete")
				self.stopped = True
				self.isrunning = False
	# end run	

# ---------- helper functions ----------
	def init_logging(self):
		# create logger
		self.logger = logging.getLogger("turtlebot." + __name__)
		self.logger.setLevel(logging.INFO)
		file_handler = None
		formatter = logging.Formatter("%(message)s")
		# create log directory
		# self.logpath = "logs/alpha_" + str(self.alpha)
		self.logpath = "logs/lernen/5_erfolge_in_simu/max/2"
		# self.logpath = "logs"
		if not os.path.isdir(self.logpath):
			os.mkdir(self.logpath)
		# create logfile
		run_count = 0
		"""self.logfile = self.logpath + "/PGSD_" + str(self.steps) + "steps_" + str(run_count) + ".log"
		while os.path.isfile(self.logfile):
			run_count += 1
			self.logfile = self.logpath + "/PGSD_" + str(self.steps) + "steps_" + str(run_count) + ".log"""
		self.logfile = "logs/video_alpha1.5_500steps.log"
		# close old logfile and handler
		if file_handler is not None:
			file_handler.close()
			self.logger.removeHandler(file_handler)
		# create new handler
		file_handler = logging.FileHandler(self.logfile, mode = "w")
		file_handler.setFormatter(formatter)
		file_handler.setLevel(logging.INFO)
		self.logger.addHandler(file_handler)
		# start actual logging
		print("Started logging to", self.logfile)
		self.logger.info("Initial Policy Parameters:\t%s %s", self.pol_param[0,0], self.pol_param[1,0])
		self.logger.info("Learning Rate:\t\t\t%s", self.alpha)
		self.logger.info("Learning Steps:\t\t\t%s", self.steps)
		self.start_time = rospy.get_time()
		self.logger.info("Start Time:\t\t\t%s\n", self.start_time)
	# end init_logging
	
	def expected_flow_from_rotation(self, ang_veloc):
		r = math.fabs(ang_veloc)
		return 0.0065*r**4 - 0.0674*r**3 + 0.1692*r**2 + 0.1954*r + 0.0192
	# end expected_flow_from_rotation
	
	# MAX flow is expected --> from head-on COLLISION test --> flow in tunnel/at angle is smaller
	def expected_flow_from_translation(self, lin_veloc):
		t = math.fabs(lin_veloc)
		return -0.1343*t**2 + 0.6317*t - 0.0557
	# end expected_flow_from_translation
	
	# ONLY use if expected flow >= 0.1629
	# MAX ALLOWED FLOW = 0.85 --> sensor limit
	def max_allowed_linear_motion_per_rotation(self, ang_veloc):
		r = self.expected_flow_from_rotation(ang_veloc)
		return (0.6317 - math.sqrt(0.5372*r - 0.08749715))/0.2686
	# end max_allowed_linear_motion_per_rotation

# ---------- callbacks ----------
	def cb_motion(self, msg):
		# accumulate state vector
		self.state[0,0] = msg.linear.x*2.4/self.tb_speed_limit	# scale up linear speed to simulation level
		self.state[1,0] = msg.angular.z
	# end cb_motion
	
	def cb_flow(self, msg):
		# direction doesn't matter, only magnitude of flow --> absolute value
		emd_count = int(len(msg.data)/2)		# msg.data = (x1,y1,x2,y2...)
		emd_count_2 = int(emd_count/2)			# emds per side
		flow_left_mat = ml.zeros((1, emd_count_2))
		flow_right_mat = ml.zeros((1, emd_count_2))
		# extract flow data from msg
		for i in range(0, emd_count, 2):
			flow_left_mat[0,int(i/2)] = math.sqrt(msg.data[i]**2 + msg.data[i+1]**2)
		for i in range(emd_count, emd_count*2, 2):
			flow_right_mat[0,int((i-emd_count)/2)] = math.sqrt(msg.data[i]**2 + msg.data[i+1]**2)
		# filter outliers outside of mean +- 2*std.dev.
		threshold_left = np.mean(flow_left_mat) + 2*np.std(flow_left_mat)
		threshold_right = np.mean(flow_right_mat) + 2*np.std(flow_right_mat)
		for i in range(emd_count_2 - 1, -1, -1):
			if flow_left_mat[0,i] > threshold_left:
				flow_left_mat = np.delete(flow_left_mat, i)
			if flow_right_mat[0,i] > threshold_right:
				flow_right_mat = np.delete(flow_right_mat, i)
		# calculate current flow
		flow_left = np.mean(flow_left_mat)
		flow_right = np.mean(flow_right_mat)
		# subtract rotation-induced flow
		rot_flow = self.expected_flow_from_rotation(self.state[1,0])
		flow_left = np.clip(flow_left - rot_flow, 0, 10)		# flow non-negative, max <= 10
		flow_right = np.clip(flow_right - rot_flow, 0, 10)		# flow non-negative, max <= 10
		# accumulate state vector
		self.state[2,0] = flow_left
		self.state[3,0] = flow_right
		flow_diff_RL = self.state[3,0] - self.state[2,0]
		if math.fabs(flow_diff_RL) < self.goal_state[2,0]:
			flow_diff_RL = self.goal_state[2,0]
		self.state[4,0] = flow_diff_RL
	# end cb_flow
	
	def cb_bumper(self, msg):
		# bumper triggered
		if msg.state == 1:
			pause = rospy.get_time() - self.stop_time
			# resume driving
			if self.stopped and pause > 2.0:
				self.stopped = False
				print("Bumper triggered: Resuming to drive.")
			# stop for safety
			else:
				self.stopped = True
				self.stop_time = rospy.get_time()
				action = Twist()
				action.linear.x = 0.0
				action.angular.z = 0.0
				self.pub_motion.publish(action)
				print("Step: " + str(self.step_count))
				print("PolParam: " + str(self.pol_param.T))
				print("Bumper triggered: TurtleBot stopped! Press Bumper again to continue.")
	# end cb_bumper
	
# end class PGSD_learner

# ---------- main function ----------
if __name__ == "__main__":
	# parser = argparse.ArgumentParser(description = __doc__)
	# parser.add_argument("-r", "--robot", dest="robot", default="atrv", help = "What type of robot: atrv, mav", type=str)
	# parser.set_defaults(mode = 0)
	# args = parser.parse_args()
	# print args
	learner = PGSD_learner()
	
	def handler(signum, frame):
		print("Signal handler called with signal", signum)
		learner.isrunning = False
		sys.exit(0)
		# raise IOError("Couldn't open device!")
	
	signal.signal(signal.SIGINT, handler)
	learner.start()	# calls run()
	
	# don't exit main before threads are cleaned up
	while True:
		# print("main loop")
		time.sleep(1)


# ---------- EXPLANATION FOR GRADIENT CALCULATION ----------
# relevant lines:
# t_g_L[0,:] *= -1
# t_g_R[0,:] *= 1

# multiplication of linspeed for left side temp gradient with -1 is necessary because of sign
# rotation has sign, flow_diff_rl has sign, but linspeed is always positive
# --> asymmetric results for left(negative) and right(positive) pol_param
# --> linspeed * -1 in temp gradient for left side corrects that = symmetric behaviour